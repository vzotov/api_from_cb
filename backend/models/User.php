<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $role
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $telephony_server
 * @property string $extension
 * @property integer $user_activity
 * @property string $accesses_moduls
 * @property string $user_type
 * @property integer $endpoint_id
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property integer $is_admin
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'password_reset_token', 'status', 'role', 'created_at', 'updated_at', 'last_name'], 'required'],
            [['status', 'role', 'created_at', 'updated_at', 'user_activity', 'endpoint_id', 'is_admin'], 'integer'],
            [['accesses_moduls'], 'string'],
            [['username', 'password_hash', 'password_reset_token', 'telephony_server', 'extension'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['email'], 'string', 'max' => 100],
            [['user_type'], 'string', 'max' => 25],
            [['first_name', 'last_name', 'middle_name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::$app->translations['id'],
            'username' => Yii::$app->translations['username'],
            'auth_key' => Yii::$app->translations['auth_key'],
            'password_hash' => Yii::$app->translations['password_hash'],
            'password_reset_token' => Yii::$app->translations['password_reset_token'],
            'email' => Yii::$app->translations['email'],
            'status' => Yii::$app->translations['status'],
            'role' => Yii::$app->translations['role'],
            'created_at' => Yii::$app->translations['created_at'],
            'updated_at' => Yii::$app->translations['updated_at'],
            'telephony_server' => Yii::$app->translations['telephony_server'],
            'extension' => Yii::$app->translations['extension'],
            'user_activity' => Yii::$app->translations['user_activity'],
            'accesses_moduls' => Yii::$app->translations['accesses_moduls'],
            'user_type' => Yii::$app->translations['user_type'],
            'endpoint_id' => Yii::$app->translations['endpoint_id'],
            'first_name' => Yii::$app->translations['first_name'],
            'last_name' => Yii::$app->translations['last_name'],
            'middle_name' => Yii::$app->translations['middle_name'],
            'is_admin' => Yii::$app->translations['is_admin'],
        ];
    }
}
