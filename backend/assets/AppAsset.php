<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.offcanvas.min.css',
        'css/uikit.min.css',
        'css/site.css',
    ];
    public $js = [
        'js/bootstrap.offcanvas.min.js',
        'js/uikit.min.js',
        'js/uikit-icons.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
}
