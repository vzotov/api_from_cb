<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .login-input{
        width: 20vmax;
    }
</style>
<form method="post" id="form-login" action="<?= Url::toRoute('/api/login'); ?>">
    <input id="form-token" type="hidden" name="<?= \Yii::$app->request->csrfParam ?>"
           value="<?= \Yii::$app->request->csrfToken ?>"/>
    <div class="site-login">
        <h3><?= Html::encode($this->title) ?></h3>
        <div class="row">
            <?php
            if (count($model->errors)):
            ?>
            <div class="col-lg-5">
                <div class="uk-alert-danger" uk-alert>
                    <a class="uk-alert-close" uk-close></a>
                    <?php
                    foreach ($model->errors as $key => $value):
                        if ($value[0] == "Incorrect username or password."):
                            ?>
                            <p><?= Yii::$app->translations[$value[0]] ?></p>
                            <?php
                        else:
                            ?>
                            <p><?= $value[0] ?></p>
                            <?
                        endif;
                    endforeach;
                    ?>
                </div>
                <?php
                endif;
                ?>
                <div class="uk-margin">
                    <div class="uk-inline">
                        <span class="uk-form-icon" uk-icon="icon: user" style="z-index: 1;"></span>
                        <input class="uk-input login-input" name="LoginForm[username]"
                               placeholder="<?= Yii::$app->translations['username'] ?>" value="<?=@$model->username?>" type="text">
                    </div>
                </div>

                <div class="uk-margin">
                    <div class="uk-inline">
                        <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: lock" style="z-index: 1;"></span>
                        <input class="uk-input login-input" name="LoginForm[password]" placeholder="<?=Yii::$app->translations['password']?>"
                               value="<?=@$model->password?>" type="password">
                    </div>
                </div>
                <button class="uk-button uk-button-primary"
                        type="submit"><?= Yii::$app->translations['exit'] ?></button>
            </div>
        </div>
    </div>
</form>

