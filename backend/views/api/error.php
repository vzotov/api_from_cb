<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
$this->title = $name;
?>
<div class="uk-flex uk-height-medium uk-margin uk-text-center" uk-grid>
    <div class="uk-tile uk-margin-auto uk-margin-auto-vertical uk-width-1-3@s uk-card uk-tile-muted" style="background-color: rgba(255,255,255,0.4)">
        <h3 class="uk-card-title"><?= Html::encode($this->title) ?></h3>
        <p><?= nl2br(Html::encode($message)) ?></p>
        <a class="uk-link-heading" href="<?=$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'];?>"><?=$_SERVER['SERVER_NAME'];?></a>
    </div>
</div>