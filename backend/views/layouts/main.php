<?php

/* @var $this \yii\web\View */

/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\Pjax;

AppAsset::register($this);
$user = Yii::$app->user->identity;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?= Yii::$app->controller->renderPartial('@app/templates/menu/index', ['user' => $user]); ?>
<progress class="uk-progress" id="js-main-progressbar" value="" max="600" style="position: fixed;top: 30px;"></progress>
<?= Yii::$app->controller->renderPartial('@app/templates/users_main_menu/index', ['user' => $user]); ?>
<?php
Pjax::begin(['id' => 'main-pjax', 'timeout' => 5000, 'enablePushState' => true, 'clientOptions' => ['method' => 'POST']]);
?>
<?php $this->beginBody() ?>

<div class="wrap" style="width: 99vmax;float: left;">

    <div class="container">
        <?= $content ?>
    </div>
</div>
<?php
Pjax::end();
$this->endBody();
?>
</body>
</html>
<?php $this->endPage() ?>
<script>
    var bar = document.getElementById('js-main-progressbar');

    function go(url) {
        $.pjax({
            type: 'POST',
            url: url,
            container: '#main-pjax',
            data: {},
            push: true,
            replace: false,
            timeout: 10000,
            "scrollTo": false
        });
        UIkit.offcanvas('#offcanvas-nav-main-menu').hide();
    }

    $(document).on('pjax:click', function () {

    });
    $(document).on('pjax:beforeSend', function () {
        bar.value += 200;
    });
    $(document).on('pjax:start', function () {
        bar.value += 200;

    });
    $(document).on('pjax:send', function () {
        bar.value += 200;
    });
    $(document).on('pjax:timeout', function () {
    });
    $(document).on('pjax:error', function () {
    });
    $(document).on('pjax:end', function () {
    });
    $(document).on('pjax:complete', function () {
    });
    $(document).on('ready pjax:end', function () {
        bar.value = 0;
    })

</script>