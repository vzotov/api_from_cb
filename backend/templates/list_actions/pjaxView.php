<?php
/**
 * @var $data
 * @var $key
 * @var $controller
 * @var $id
 * @var $options
 */

use yii\helpers\Url;

$modulsAccesses = unserialize(\Yii::$app->getUser()->identity->accesses_moduls);
?>
<div class="btn-group" style="float: right">
<?php
$activity = $data->tableName().'_activity';
if ($data->$activity):?>
    <button type="button" style="padding: 5px 8px 5px 8px;" onclick="activity_<?=$id;?>('<?=$data->$key; ?>')" class="btn btn-success">
        <span class="glyphicon glyphicon-ok-circle"></span>
    </button>
<?php else:?>
    <button type="button" style="padding: 5px 8px 5px 8px;" onclick="activity_<?=$id;?>('<?=$data->$key; ?>')" class="btn btn-warning">
        <span class="glyphicon glyphicon-remove-circle"></span>
    </button>
<?php endif;?>
    <?php
    if (@$modulsAccesses[$modul]['view']):
    ?>
    <a type="button" href="<?php echo Url::toRoute('/' . $modul . '/' . $controller . '/viewpj?id=' . $data->$key.@$options['return']);?>" style="padding: 5px 8px 5px 8px;" class="btn btn-info">
        <span class="glyphicon glyphicon-file"></span>
    </a>
    <?php
    endif;
    if (@$modulsAccesses[$modul]['update']):
    ?>
    <a type="button" href="<?php echo Url::toRoute('/' . $modul . '/' . $controller . '/update?id=' . $data->$key.@$options['return']);?>" style="padding: 5px 8px 5px 8px;" class="btn btn-primary">
        <span class="glyphicon glyphicon-pencil"></span>
    </a>
    <?php
    endif;
    if (@$modulsAccesses[$modul]['deleted']):
    ?>
    <button type="button" style="padding: 5px 8px 5px 8px;" onclick="delete_<?=$id;?>('<?=$data->$key;?>')" class="btn btn-danger">
        <span class="glyphicon glyphicon-trash"></span>
    </button>
    <?php
    endif;
    ?>
</div>