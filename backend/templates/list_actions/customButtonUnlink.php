<?php
/**
 * @var $data
 * @var $id
 * @var $js_method
 */

use yii\helpers\Url;
use backend\extension\pbxUrlReturn;

?>
<div class="btn-group" >
    <?php
    if (Yii::$app->getUser()->identity->is_admin == '1'):
        ?>
        <button type="button" style="padding: 5px 8px 5px 8px;"
                onclick="<?= @$js_method ?>('<?= @$data[$id]; ?>')"
                class="btn btn-danger">
            <span class="glyphicon glyphicon-resize-full"></span>
        </button>
    <?php
    endif;
    ?>
</div>


