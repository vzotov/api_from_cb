<?php
/**
 * @var $data
 * @var $id
 * @var $field
 * @var $js_method
 */

use yii\helpers\Url;
use backend\extension\pbxUrlReturn;

?>
<div class="btn-group">
    <?php
    if (@$data[@$field]):?>
        <button type="button" style="padding: 5px 8px 5px 8px;" onclick="<?=@$js_method?>(<?=@$data[@$id]?>)"
                class="btn btn-success">
            <span class="glyphicon glyphicon-ok-circle"></span>
        </button>
    <?php else: ?>
        <button type="button" style="padding: 5px 8px 5px 8px;" onclick="<?=@$js_method?>(<?=@$data[@$id]?>)"
                class="btn btn-warning">
            <span class="glyphicon glyphicon-remove-circle"></span>
        </button>
    <?php endif; ?>
</div>


