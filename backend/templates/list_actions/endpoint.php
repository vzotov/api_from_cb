<?php
/**
 * @var $modul
 * @var $endpoint
 * @var $amiStat
 */

use yii\helpers\Url;

switch ($amiStat['DeviceState']) {
    case 'In use':
        $css = 'endpoint-label-in-use';
        $tooltip = Yii::$app->translator($amiStat['DeviceState']);
        break;
    case 'Not in use':
        $css = 'endpoint-label-not-in-use';
        $tooltip = Yii::$app->translator($amiStat['DeviceState']);
        break;
    case 'Unavailable':
        $css = 'endpoint-label-unavailable';
        $tooltip = Yii::$app->translator($amiStat['DeviceState']);
        break;
    case 'Ringing':
        $css = 'endpoint-label-ringing';
        $tooltip = Yii::$app->translator($amiStat['DeviceState']);
        break;
    case 'Not found':
        $css = 'endpoint-label-not-found';
        $tooltip = Yii::$app->translator($amiStat['DeviceState']);
        break;
    case 'Invalid':
        $css = 'endpoint-label-invalid';
        $tooltip = Yii::$app->translator($amiStat['DeviceState']);
        break;
    default:
        $css = 'endpoint-label';
        $tooltip = '??????';
        break;
}
?>
<div class="<?=$css;?>" uk-tooltip="<?=$tooltip;?>" >
<?php
echo $endpoint;
?>
</div>
<a href="" ONCLICK="local_call_<?=@$modul;?>('<?=$endpoint;?>')" uk-icon="receiver"></a>