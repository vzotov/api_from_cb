<?php
/**
 * @var $value
 * @var $id
 * @var $name
 * @var $js_method
 * @var $option
 * @var $color
 */

use yii\helpers\Url;
use backend\extension\pbxUrlReturn;

?>
<div class="uk-margin">
    <div uk-form-custom="target: > * > span:last-child">
        <select id="<?=$name.@$id?>" onchange="<?=@$js_method?>('<?=@$name?>','<?=$id?>')">
            <?php
            foreach (@$option as $key=>$val):
                $selected = '';
                if ((string)$key==@$value[@$name]) {
                    $selected = 'selected';
                }
                ?>
                <option <?=@$selected?> value="<?=$key?>"><?=$val?></option>
            <?php
            endforeach;
            ?>
        </select>
        <span class="uk-link uk-label <?=@$color[@$value[@$name]]?>">
                <span></span>
            </span>
    </div>
</div>
