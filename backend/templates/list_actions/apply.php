<?php
/**
 * @var $data
 * @var $key
 * @var $controller
 * @var $id
 * @var $options
 */

use yii\helpers\Url;

?>
<div class="btn-group" style="float: right">
<?php
$activity = $data->tableName().'_apply';
if ($data->$activity):?>
    <button type="button" style="padding: 5px 8px 5px 8px;" onclick="apply_<?=@$id;?>('<?=@$data->$key; ?>')" class="btn btn-success">
        <span class="glyphicon glyphicon-cloud"></span>
    </button>
<?php else:?>
    <button type="button" style="padding: 5px 8px 5px 8px;" onclick="apply_<?=@$id;?>('<?=@$data->$key; ?>')" class="btn btn-warning">
        <span class="glyphicon glyphicon-cloud"></span>
    </button>
<?php endif;?>
</div>