<?php
/**
 * @var $data
 * @var $rhName
 * @var $rhID
 * @var $rhModel
 * @var $rhModule
 */

use yii\helpers\Url;
use backend\extension\pbxUrlReturn;

/*
 * URL куда нужно вернуться
 */

$returnUrl = @$_SERVER['REDIRECT_URL'] . '?' . @$_SERVER['REDIRECT_QUERY_STRING'];

/*
 * Добавляем к URL параметр return
 */

$returnParams = pbxUrlReturn::returnUrl();

if (is_object($rhModel)):
?>
<a class="uk-link" href="<?php echo Url::toRoute('/' . @$rhModule . '/default/view?id=' . @$rhModel->$rhID.$returnParams);?>">
   <span uk-icon="icon: link; ratio: 1"> <?=@$rhModel->$rhName;?></span>
</a>
<?php
else:
?>
    <span class="uk-label uk-label-danger"><?=yii::$app->translator('not found')?></span>
<?php
endif;
 ?>
