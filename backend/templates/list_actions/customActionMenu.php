<?php
/**
 * @var $data
 * @var $id
 * @var $js_method
 */

use yii\helpers\Url;
use backend\extension\pbxUrlReturn;

?>
<div class="btn-group">
    <?php
    if (Yii::$app->getUser()->identity->is_admin == '1'):
        ?>
        <a type="button" style="padding: 2px 5px 7px 6px"
           href="#modal-overflow<?= $data['role_action_id'] ?>"
           uk-toggle
           class="btn uk-button-primary">
            <span class="glyphicon" uk-icon="icon: more-vertical;"></span>
        </a>
    <?php
    endif;
    ?>
</div>

<div id="modal-overflow<?= $data['role_action_id'] ?>" uk-modal>
    <div class="uk-modal-dialog">

        <button class="uk-modal-close-default" type="button" uk-close></button>

        <div class="uk-modal-header">
            <h2 class="uk-modal-title"><?= Yii::$app->translator($data['role_action_modul_name']) ?></h2>
        </div>
        <form class="role-action" action="<?= Url::toRoute('/role/default/modactions'); ?>" method="post">
            <input name="id" type="hidden" value="<?= $data['role_action_id']; ?>">
            <input id="form-token" type="hidden" name="<?= \Yii::$app->request->csrfParam ?>"
                   value="<?= \Yii::$app->request->csrfToken ?>"/>
            <div class="uk-modal-body" uk-overflow-auto>
                <table class="uk-table uk-table-responsive uk-table-divider">
                    <thead>
                    <tr>
                        <th><?= Yii::$app->translator('actions') ?></th>
                        <th><?= Yii::$app->translator('access') ?></th>
                        <th><?= Yii::$app->translator('additionally') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $roleActionModAction = json_decode($data['role_action_mod_action'], true);
                    if (is_array($roleActionModAction)) {
                        foreach (@$roleActionModAction as $key => $value):
                            ?>
                            <tr>
                                <td><?= Yii::$app->translator($key) ?></td>
                                <td>
                                    <?php if ($value): ?>
                                        <input class="uk-checkbox" type="checkbox" name="role_action[<?= @$key ?>]" checked>
                                    <?php else: ?>
                                        <input class="uk-checkbox" name="role_action[<?= @$key ?>]" type="checkbox">
                                    <?php endif; ?>
                                </td>
                                </td>
                                <td><span class="uk-label"><?= Yii::$app->translator('no_data') ?></span></td>
                            </tr>
                        <?php
                        endforeach;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button class="uk-button uk-button-default uk-modal-close"
                        type="button"><?= Yii::$app->translator('close') ?></button>
                <button class="uk-button uk-button-primary"
                        type="submit"><?= Yii::$app->translator('action_save') ?></button>
            </div>
        </form>
    </div>
</div>

