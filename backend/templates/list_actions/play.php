<?php
/**
 * @var $data array
 * @var $modul
 * @var $options
 */

use yii\helpers\Url;
use backend\extension\pbxUrlReturn;

/*
 * формируем параметры url для возврата на форму связанного модуля
 */
$returnParams = pbxUrlReturn::returnUrl();

/*
 * Параметры URL формы где находимся
 */
$thisUrl = pbxUrlReturn::thisPlace();

/*
 * Названия контролера
 */
$controller = 'default';

/*
 * id сущности
 */
$tuId = $data[$modul.'_id'];
?>

    <?php
    if (Yii::$app->permissionCheck($modul, 'Play') || Yii::$app->getUser()->identity->is_admin == '1'):
        ?>
        <a href="" class="btn btn-success"
           style="background-color: #4cae4c;padding: 2px 8px 4px 8px;color: #fff"
           uk-icon="icon: play;ratio: 1.5"
           uk-toggle="target: #offcanvas-usage">

        </a>
    <?php
    else:
        ?>
        <a href="" class="btn btn-success"
           style="background-color: #4cae4c;padding: 2px 8px 4px 8px;color: #fff"
           uk-icon="icon: play;ratio: 1.5"
        </a>
    <?php
    endif;
    ?>


