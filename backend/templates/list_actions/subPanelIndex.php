<?php
/**
 * @var $data array
 * @var $column
 * @var $modul
 * @var $options
 */

use yii\helpers\Url;
use backend\extension\pbxUrlReturn;
/*
 * id связанной сущности
 */
$tuId = @$data[$column->subpanels['relation']['to']];

/*
 * id сущности
 */
$fromId = @$data[$column->subpanels['relation']['from']];

/*
 * id связи
 */
$unlinkId = @$data[$column->subpanels['relation']['cross-id']];

/*
 * формируем параметры url для возврата на форму связанного модуля
 */
$returnParams = pbxUrlReturn::returnUrl();

/*
 * Параметры URL формы где находимся
 */
$thisUrl = pbxUrlReturn::thisPlace();

/*
 * названия связанной сущности
 */
$rhModul = @$column->subpanels['moduls'];

/*
 * Названия параметра активность связанной сущности
 */
$activity = $rhModul.'_activity';

/*
 * название id связанной сущности
 */
$id = $rhModul.'_id';

/*
 * Названия контролера
 */
$controller = 'default';

/**
 * id субпанели
 */
$subPanelId = @$column->subpanels['id'];

?>
<div class="btn-group" style="float: right">
    <?php
    if (Yii::$app->permissionCheck($rhModul, 'Activity') || Yii::$app->getUser()->identity->is_admin == '1') {
        $onclick = 'onclick="activity' . $subPanelId . '(\'' . $data[$id] . '\')"';
    } else {
        $onclick = 'disabled';
    }
    if ($data[$activity]):?>
        <button type="button" style="padding: 5px 8px 5px 8px;" <?=$onclick;?>
                class="btn btn-success">
            <span class="glyphicon glyphicon-ok-circle"></span>
        </button>
    <?php else: ?>
        <button type="button" style="padding: 5px 8px 5px 8px;" <?=$onclick;?>
                class="btn btn-warning">
            <span class="glyphicon glyphicon-remove-circle"></span>
        </button>
    <?php endif; ?>

    <?php
    if (Yii::$app->permissionCheck($rhModul, 'View') || Yii::$app->getUser()->identity->is_admin == '1'):
        ?>
        <a type="button"
           href="<?php echo Url::toRoute('/' . $rhModul . '/' . $controller . '/view?id=' . $tuId . $returnParams); ?>"
           style="padding: 5px 8px 5px 8px;" class="btn btn-info">
            <span class="glyphicon glyphicon-file"></span>
        </a>
    <?php
    else:
        ?>
        <a type="button"
           disabled=""
           style="padding: 5px 8px 5px 8px;" class="btn btn-info">
            <span class="glyphicon glyphicon-file"></span>
        </a>
    <?php
    endif;

    if (Yii::$app->permissionCheck($rhModul, 'Update') || Yii::$app->getUser()->identity->is_admin == '1'):
        ?>
        <a type="button"
           href="<?php echo Url::toRoute('/' . $rhModul . '/' . $controller . '/update?id=' . $tuId . $returnParams); ?>"
           style="padding: 5px 8px 5px 8px;" class="btn btn-primary">
            <span class="glyphicon glyphicon-pencil"></span>
        </a>
    <?php
    else:
        ?>
        <a type="button"
           disabled=""
           style="padding: 5px 8px 5px 8px;" class="btn btn-primary">
            <span class="glyphicon glyphicon-pencil"></span>
        </a>
    <?php
    endif;
    $unLinkActionName = @$column->subpanels['unLinkName'];

    if (Yii::$app->permissionCheck($rhModul, $unLinkActionName) || Yii::$app->getUser()->identity->is_admin == '1'):
        ?>
        <button type="button" style="padding: 5px 8px 5px 8px;" onclick="unlink<?= @$subPanelId; ?>('<?= @$unlinkId; ?>')"
                class="btn btn-danger">
            <span class="glyphicon glyphicon-resize-full"></span>
        </button>
    <?php
    else:
        ?>
        <button type="button" style="padding: 5px 8px 5px 8px;" disabled
                class="btn btn-danger">
            <span class="glyphicon glyphicon-resize-full"></span>
        </button>
    <?php
    endif;
    ?>
</div>


