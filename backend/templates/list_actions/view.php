<?php
/**
 * @var $data array
 * @var $modul
 * @var $options
 */

use yii\helpers\Url;
use backend\extension\pbxUrlReturn;

/*
 * формируем параметры url для возврата на форму связанного модуля
 */
$returnParams = pbxUrlReturn::returnUrl();

/*
 * Параметры URL формы где находимся
 */
$thisUrl = pbxUrlReturn::thisPlace();

/*
 * Названия контролера
 */
$controller = 'default';

/*
 * id сущности
 */
$tuId = $data[$modul.'_id'];
?>
<div class="btn-group" style="float: right">
    <?php
    if (Yii::$app->permissionCheck($modul, 'View') || Yii::$app->getUser()->identity->is_admin == '1'):
        ?>
        <a type="button"
           href="<?php echo Url::toRoute('/' . $modul . '/' . $controller . '/view?id=' . $tuId . $returnParams); ?>"
           style="padding: 5px 8px 5px 8px;" class="btn btn-info">
            <span class="glyphicon glyphicon-file"></span>
        </a>
    <?php
    else:
        ?>
        <a type="button"
           disabled=""
           style="padding: 5px 8px 5px 8px;" class="btn btn-info">
            <span class="glyphicon glyphicon-file"></span>
        </a>
    <?php
    endif;
    ?>
</div>


