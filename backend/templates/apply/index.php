<?php
/**
 * @var $modul
 * @var $model
 */
$fildApply = @$modul . '_apply';
$fildId = @$modul . '_id';

?>

<div class="btn-group">
    <?php
    if (@$model->$fildApply && (Yii::$app->getUser()->identity->is_admin == '1' || Yii::$app->permissionCheck($modul, 'Apply'))):?>
        <button type="submit" style="padding: 5px 8px 5px 8px;" class="btn btn-success">
            <span class="glyphicon glyphicon-cloud"></span>
        </button>
    <?php elseif (@!$model->$fildApply && (Yii::$app->getUser()->identity->is_admin == '1' || Yii::$app->permissionCheck($modul, 'Apply'))): ?>
        <form id="apply<?= @$model->$fildId ?>" action="/<?= @$modul ?>/default/apply" method="post">
            <input id="form-token" type="hidden" name="<?= \Yii::$app->request->csrfParam ?>"
                   value="<?= \Yii::$app->request->csrfToken ?>"/>
            <input name="id" type="hidden" value="<?= @$model->$fildId ?>">
            <button type="submit" style="padding: 5px 8px 5px 8px;" class="btn btn-warning">
                <span class="glyphicon glyphicon-cloud"></span>
            </button>
        </form>
        <script>
            $(function () {
                $('#apply<?= @$model->$fildId ?>').submit(function (e) {
                    var $form = $(this);
                    $.ajax({
                        type: $form.attr('method'),
                        url: $form.attr('action'),
                        data: $form.serialize()
                    }).done(function (data) {
                        go('');
                        console.log(data);
                    }).fail(function () {
                        console.log('fail');
                    });
                    e.preventDefault();
                });
            });
        </script>
        <?php else : ?>
        <?php
        if (@$model->$fildApply):
        ?>
            <button type="submit" style="padding: 5px 8px 5px 8px;" class="btn btn-success" disabled>
                <span class="glyphicon glyphicon-cloud"></span>
            </button>
        <?php
        else:
            ?>
            <button type="submit" style="padding: 5px 8px 5px 8px;" class="btn btn-warning" disabled>
                <span class="glyphicon glyphicon-cloud"></span>
            </button>
        <?php
        endif;
    endif;
    ?>
</div>
