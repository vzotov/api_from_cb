<?php
/**
 * @var $modul
 * @var $data array
 * @var $column
 * @var $options
 */
/**
 * Название атрибута сущности
 */
$fildApply = @$modul.'_apply';

/**
 * Название атрибута идентификатора сущности
 */
$fildId = @$modul.'_id';

?>

<div class="btn-group">
    <?php
    if (Yii::$app->permissionCheck($modul, 'Apply') || Yii::$app->getUser()->identity->is_admin == '1'){
        $disabled = '';
    }else{
        $disabled = 'disabled';
    }
    if (@$data[$fildApply]):?>
        <button type="submit" style="padding: 5px 8px 5px 8px;" class="btn btn-success" <?=$disabled?>>
            <span class="glyphicon glyphicon-cloud"></span>
        </button>
    <?php else: ?>
        <form id="apply<?= @$data[$fildId] ?>" class="apply<?=@$column->subpanels['id'] ?>"
              action="/<?= @$modul ?>/default/apply" method="post">
            <input id="form-token" type="hidden" name="<?= \Yii::$app->request->csrfParam ?>"
                   value="<?= \Yii::$app->request->csrfToken ?>"/>
            <input name="id" type="hidden" value="<?=@$data[$fildId]?>">
            <button type="submit" style="padding: 5px 8px 5px 8px;" class="btn btn-warning" <?=$disabled?>>
                <span class="glyphicon glyphicon-cloud"></span>
            </button>
        </form>
    <?php endif; ?>
</div>
