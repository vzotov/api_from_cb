<?php

use common\extension\AmiAction;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\db\Query;

?>
<script>
    function ExtensionWidget(data) {
        jQuery.pjax.defaults.timeout = 5000;
        jQuery.pjax.reload({container: '#WidgetExtensionPjax'});
        $("#spinner-widget-extension").show();
        //$('.js-filter').removeAttr( 'style' )
        setTimeout(function () {
            $("#spinner-widget-extension").hide();
        }, 1000);
    }

    function go(url) {
        $.pjax({
            type: 'POST',
            url: url,
            container: '#WidgetExtensionPjax',
            data: {},
            push: true,
            replace: false,
            timeout: 10000,
            "scrollTo": false
        });
    }

    function filter(name) {
        $.ajax({
            type: 'get',
            url: "<?=Url::toRoute('/widget/session');?>",
            data: 'params=filter&data=' + name
        }).done(function (data) {
            console.log(data);
        }).fail(function () {
            console.log('fail');
        });
    }

    function c2c(ext){
        $.ajax({
            type: 'get',
            url: "<?=Url::toRoute('/widget/localcall');?>",
            data: 'endpoint=' + ext
        }).done(function (data) {
            console.log(data);
        }).fail(function () {
            console.log('fail');
        });
    }
</script>
<?php
Pjax::begin(['id' => 'WidgetExtensionPjax', 'timeout' => 5000, 'enablePushState' => true, 'clientOptions' => ['method' => 'POST']]);
$session = \Yii::$app->session;
$filter = unserialize(@$session['filter']);
$endpoints = AmiAction::PJSIPShowEndpoints();
?>
<span id="spinner-widget-extension" class="uk-margin-small-right" uk-spinner="ratio: 0.5"></span>
<div uk-filter="target: .js-filter">

    <ul class="uk-subnav uk-subnav-pill">
        <li onclick="filter('All')" <?php if ($filter == 'All') echo 'class="uk-active"'; ?> uk-filter-control><a
                    href="#">All</a></li>
        <li onclick="filter('In use')" <?php if ($filter == 'In use') echo 'class="uk-active"'; ?>
            uk-filter-control="[data-color='In use']"><a href="#"><?= Yii::$app->translator('In use') ?></a></li>
        <li onclick="filter('Not in use')" <?php if ($filter == 'Not in use') echo 'class="uk-active"'; ?>
            uk-filter-control="[data-color='Not in use']"><a href="#"><?= Yii::$app->translator('Not in use') ?></a>
        </li>
        <li onclick="filter('Unavailable')" <?php if ($filter == 'Unavailable') echo 'class="uk-active"'; ?>
            uk-filter-control="[data-color='Unavailable']"><a href="#"><?= Yii::$app->translator('Unavailable') ?></a>
        </li>
        <li onclick="filter('Ringing')" <?php if ($filter == 'Ringing') echo 'class="uk-active"'; ?>
            uk-filter-control="[data-color='Ringing']"><a href="#"><?= Yii::$app->translator('Ringing') ?></a></li>
    </ul>

    <ul class="js-filter uk-child-width-1-2 uk-child-width-1-3@m uk-text-center" uk-grid>
        <?php
        foreach ($endpoints as $key => $value):
            switch ($value['DeviceState']) {
                case 'In use':
                    $background = '#02ff21';
                    $tooltip = Yii::$app->translator($value['DeviceState']);
                    break;
                case 'Not in use':
                    $background = '#487edc';
                    $tooltip = Yii::$app->translator($value['DeviceState']);
                    break;
                case 'Unavailable':
                    $background = '#8c8c8c';
                    $tooltip = Yii::$app->translator($value['DeviceState']);
                    break;
                case 'Ringing':
                    $background = '#ff8e03';
                    $tooltip = Yii::$app->translator($value['DeviceState']);
                    break;
                case 'Not found':
                    $background = '#02ff21';
                    $tooltip = Yii::$app->translator($value['DeviceState']);
                    break;
                case 'Invalid':
                    $css = 'endpoint-label-invalid';
                    $background = '#e00000';
                    $tooltip = Yii::$app->translator($value['DeviceState']);
                    break;
                default:
                    $background = '#8c8c8c';
                    $tooltip = '??????';
                    break;
            }
            $endpoint = new Query();
            $endpoint->select('*')->from('endpoint')->one();
            $endpoint->andWhere(['=', 'endpoint_aors', $key]);
            ?>
            <li data-color="<?= $value['DeviceState'] ?>">
                <div class="uk-card uk-card-default uk-card-body"
                     style="color: #fff;position: relative;background-color:<?= $background ?>">
                    <?= $endpoint->one()['endpoint_last_name'] ?> <?= $endpoint->one()['endpoint_first_name'] ?>
                    [<?= $key ?>]<br><?= $tooltip ?>
                    <span class="uk-margin-small-right" onclick="c2c('<?=$key?>')"
                          style="position: absolute;top: 4px;right: 0px;cursor: pointer;" uk-icon="receiver"></span></div>
            </li>
        <?php
        endforeach;
        ?>
    </ul>

</div>
<?php
Pjax::end();
?>

<!--<span id="name-widget-channels" class="uk-text-lead" style="color: #fff">Активные каналы:</span>-->
