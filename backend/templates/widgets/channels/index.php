<?php
use common\extension\AmiAction;
use yii\widgets\Pjax;
use yii\helpers\Url;
?>
<script>
    function ChannelsWidget(data) {
        <?php
        if (!strlen(@$_REQUEST['redirect'])):
        ?>
        jQuery.pjax.defaults.timeout = 5000;
        jQuery.pjax.reload({container: '#ChannelsWidgetPjax'});
        $( "#spinner-widget-channels" ).show();
        setTimeout(function(){
            $( "#spinner-widget-channels" ).hide();
        },1000);
        <?php
            endif;
        ?>
    }

    function hangup(channel) {
        $.ajax({
            type: 'get',
            url: "<?=Url::toRoute('/widget/hangup');?>",
            data: 'channel=' + channel
        }).done(function (data) {
            console.log(data);
            jQuery.pjax.defaults.timeout = 5000;
            jQuery.pjax.reload({container: '#ChannelsWidgetPjax'});
        }).fail(function () {
            console.log('fail');
        });
    }

    function ChanSpy(ext) {
        $.ajax({
            type: 'get',
            url: "<?=Url::toRoute('/widget/chanspy');?>",
            data: 'ext=' + ext
        }).done(function (data) {
            console.log(data);
            jQuery.pjax.defaults.timeout = 5000;
            jQuery.pjax.reload({container: '#ChannelsWidgetPjax'});
        }).fail(function () {
            console.log('fail');
        });
    }

    function redirect(ext,channel,extracHannel){
        $.ajax({
            type: 'get',
            url: "<?=Url::toRoute('/widget/redirect');?>",
            data: 'ext=' + ext +'&channel='+ channel+'&extracHannel='+extracHannel
        }).done(function (data) {
            console.log(data);
            jQuery.pjax.defaults.timeout = 5000;
            jQuery.pjax.reload({container: '#ChannelsWidgetPjax'});
        }).fail(function () {
            console.log('fail');
        });

    }

    function go(url) {
        $.pjax({
            type: 'POST',
            url: url,
            container: '#ChannelsWidgetPjax',
            data: {},
            push: true,
            replace: false,
            timeout: 10000,
            "scrollTo": false
        });
    }


</script>
<?php
Pjax::begin(['id' => 'ChannelsWidgetPjax', 'timeout' => 5000, 'enablePushState' => true, 'clientOptions' => ['method' => 'POST']]);
if (@$_REQUEST['redirect'] == 'extlist') {
    echo Yii::$app->controller->renderPartial('@app/templates/widgets/channels/extlist', ['channels' => $_REQUEST]);
} else {
    $channels = AmiAction::CoreShowChannels();
    $calls['out'] = [];
    $calls['in'] = [];
    foreach ($channels as $key => $value) {
        if ($value['Linkedid'] == $key) {
            $calls['out'] = $value;
            foreach ($channels as $val) {
                if ($val['Linkedid'] == $key && $value['CallerIDNum'] != $val['CallerIDNum']) {
                    $calls['in'] = $val;
                }
            }
            echo Yii::$app->controller->renderPartial('@app/templates/widgets/channels/channel-item', ['calls' => $calls]);
        }
    }
}
Pjax::end();
?>
<span id="spinner-widget-channels" class="uk-margin-small-right" uk-spinner="ratio: 0.5"></span>
<!--<span id="name-widget-channels" class="uk-text-lead" style="color: #fff">Активные каналы:</span>-->
