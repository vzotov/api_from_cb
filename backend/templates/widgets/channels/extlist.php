<?php

use common\extension\AmiAction;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\db\Query;

/**
 * @var $channels
 */

?>
<div style="width: 100%;border-bottom: 1px solid #fff">
    <button type="button" style="padding: 5px 8px 5px 8px;background-color: rgba(0, 120, 201, 0)"
            onclick="go('<?= Url::toRoute('/widget/channels') ?>')"
            href="#modal-sections" uk-toggle class="btn btn-outline-success">
        <h4 style="color: #fff;margin-bottom: 5px;" class=""><i class="glyphicon glyphicon-chevron-left"></i><?= Yii::$app->translator('backwards') ?></h4>
    </button>
</div>

<div style="width: 100%;border-bottom: 1px solid #fff;margin-top: 25px">
    <h5 style="color: #dddcdc;margin-bottom:15px" class=""><?= Yii::$app->translator('queues') ?>:</h5>
    <?php
    $queues = new Query();
    $queues->select('*')->from('queues')->all();
    $queues->andWhere(['=', 'queues_activity', '1']);
    foreach (@$queues->all() as $val):
        ?>
        <table class="uk-table" style="color: unset;padding: 5px 3px;">
            <tr style="background-color: rgba(0, 120, 201, 0)">
                <td style="padding: 5px 3px;">
                <span style="color: #fff;cursor: pointer"
                      onclick="redirect('q<?=@$val['queues_name']?>','<?=@$channels['channel']?>','<?=@$channels['ExtraChannel']?>');"
                      class="uk-text-large">
                    <?= $val['queues_description'] ?> <b>[<?= $val['queues_name'] ?>]</b>
                </span>
                </td>
            </tr>
        </table>
    <?php
    endforeach;
    ?>
</div>

<div style="width: 100%;border-bottom: 1px solid #fff;margin-top: 25px">
    <h5 style="color: #dddcdc;margin-bottom:15px" class=""><?= Yii::$app->translator('endpoint') ?>:</h5>
    <table class="uk-table" style="color: unset;padding: 5px 3px;">
        <?php
        $endpointStat = AmiAction::PJSIPShowEndpoints();
        $endpoint = new Query();
        $endpoint->select('*')->from('endpoint')->all();
        $endpoint->andWhere(['=', 'endpoint_activity', '1']);
        foreach (@$endpoint->all() as $val):
            ?>
            <tr style="background-color: rgba(0, 120, 201, 0)">
                <td style="padding: 5px 3px;">
                <span style="color: #fff;cursor: pointer"
                      onclick="redirect('e<?=$val['endpoint_aors']?>','<?=@$channels['channel']?>','<?=@$channels['ExtraChannel']?>');"
                      class="uk-text-large">
                    <?= $val['endpoint_last_name'] ?> <?= $val['endpoint_first_name'] ?> <b>[<?= $val['endpoint_aors'] ?>]</b>
                </span>
                </td>
                <td style="padding: 5px 3px;">
                <span style="color: #fff;cursor: pointer" class="uk-text-large">
                    <?= $val['endpoint_department'] ?>
                </span>
                </td>
                <td style="padding: 5px 3px;">
                <span style="color: #fff;cursor: pointer" class="uk-text-large">
                    <?= Yii::$app->translator($endpointStat[$val['endpoint_aors']]['DeviceState']) ?>
                </span>
                </td>
            </tr>
        <?php
        endforeach;
        ?>
    </table>
</div>
<div style="width: 100%;">
    <button type="button" style="padding: 5px 8px 5px 8px;background-color: rgba(0, 120, 201, 0)"
            onclick="go('<?= Url::toRoute('/widget/channels') ?>')"
            href="#modal-sections" uk-toggle class="btn btn-outline-success">
        <h4 style="color: #fff;margin-bottom: 5px;" class=""><i class="glyphicon glyphicon-chevron-left"></i><?= Yii::$app->translator('backwards') ?></h4>
    </button>
</div>