<?php

use common\extension\AmiAction;
use yii\widgets\Pjax;
use yii\helpers\Url;
/**
 * @var $calls
 */

if (!count($calls['in'])):
    ?>
    <table class="uk-table" style="color: unset;border-radius: 2px;border-bottom: 1px solid #fff;padding: 5px 3px;">
        <tr style="background-color: rgba(0, 120, 201, 0)">
            <td style="padding: 5px 3px;">
                <span style="color: #fff;" class="uk-text-large">
                <?= $calls['out']['ChannelStateDesc'] ?>
                    <?= $calls['out']['CallerIDName'] ?> [<?= $calls['out']['CallerIDNum'] ?>]
                 => Ext(<?= $calls['out']['Exten'] ?>) => <?= $calls['out']['ConnectedLineName'] ?> [<?= $calls['out']['ConnectedLineNum'] ?>] ...
                </span>
            </td>
            <td style="width: 150px;padding: 5px 3px;">
                <div class="btn-group" style="float: right;background-color: rgba(0, 120, 201, 0)">
                    <button type="button" style="padding: 5px 8px 5px 8px;background-color: rgba(0, 120, 201, 0)"
                            onclick="hangup('<?=$calls['out']['Channel']?>');" class="btn btn-outline-success">
                        <span style="color: #fff" class="glyphicon glyphicon-off"></span>
                    </button>
                    <button type="button" style="padding: 5px 8px 5px 8px;background-color: rgba(0, 120, 201, 0)"
                            onclick="ChanSpy('<?=$calls['out']['CallerIDNum']?>')" class="btn btn-outline-success">
                        <span style="color: #fff" class="glyphicon glyphicon-headphones"></span>
                    </button>
                    <button type="button" style="padding: 5px 8px 5px 8px;background-color: rgba(0, 120, 201, 0)"
                            onclick="go('<?= Url::toRoute('/widget/channels') . '?redirect=extlist&channel=' . $calls['out']['Channel'] . '&ExtraChannel='; ?>')"
                            href="#modal-sections" uk-toggle class="btn btn-outline-success">
                        <span style="color: #fff" class="glyphicon glyphicon-random"></span>
                    </button>
                </div>
            </td>
        </tr>
    </table>
<?php
else:
    ?>
    <table class="uk-table" style="color: unset;border-radius: 2px;border-bottom: 1px solid #fff;padding: 5px 3px;">
        <tr style="background-color: rgba(0, 120, 201, 0)">
            <td style="padding: 5px 3px;">
                <span style="color: #fff;" class="uk-text-large">
                <?= $calls['out']['ChannelStateDesc'] ?>
                    <?= $calls['out']['CallerIDName'] ?> [<?= $calls['out']['CallerIDNum'] ?>]
                 => Ext(<?= $calls['out']['Exten'] ?>) =>
                    <?= $calls['in']['ChannelStateDesc'] ?>
                    <?= $calls['in']['CallerIDName'] ?> [<?= $calls['in']['CallerIDNum'] ?>] ...
                </span>
            </td>
            <td style="width: 150px;padding: 5px 3px;">
                <div class="btn-group" style="float: right;background-color: rgba(0, 120, 201, 0)">
                    <button type="button" style="padding: 5px 8px 5px 8px;background-color: rgba(0, 120, 201, 0)"
                            onclick="hangup('<?=$calls['out']['Channel']?>');" class="btn btn-outline-success">
                        <span style="color: #fff" class="glyphicon glyphicon-off"></span>
                    </button>
                    <button type="button" style="padding: 5px 8px 5px 8px;background-color: rgba(0, 120, 201, 0)"
                            onclick="ChanSpy('<?=$calls['out']['CallerIDNum']?>')" class="btn btn-outline-success">
                        <span style="color: #fff" class="glyphicon glyphicon-headphones"></span>
                    </button>
                    <button type="button" style="padding: 5px 8px 5px 8px;background-color: rgba(0, 120, 201, 0)"
                            onclick="go('<?= Url::toRoute('/widget/channels') . '?redirect=extlist&channel=' . $calls['out']['Channel'] . '&ExtraChannel='.$calls['in']['Channel']; ?>')"
                            href="#modal-sections" uk-toggle class="btn btn-outline-success">
                        <span style="color: #fff" class="glyphicon glyphicon-random"></span>
                    </button>
                </div>
            </td>
        </tr>
    </table>
<?php
endif;
?>
