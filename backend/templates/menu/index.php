<?php
/**
 * @var $user
 */

use yii\helpers\Url;

?>

<nav class="main-menu uk-navbar-container" style="background-image: url('/./img/meadow-2184989_1920.jpg');">
    <div class="uk-navbar-left">
        <ul class="uk-navbar-nav">
            <li>
                <div class="uk-navbar-left" uk-toggle="target: #offcanvas-nav-main-menu">
                    <a class="uk-navbar-toggle">
                        <span uk-navbar-toggle-icon></span> <span class="uk-margin-small-left">Menu</span>
                    </a>
                </div>
            </li>
            <li>
                <div class="uk-navbar-left">
                    <a class="uk-navbar-toggle" href="<?= Url::toRoute('/api/index'); ?>">
                        <span uk-icon="home"></span> <span class="uk-margin-small-left">Home</span>
                    </a>
                </div>
            </li>

        </ul>
        <ul class="uk-navbar-nav" style="right: 0vmax;position: absolute;cursor: pointer;">
            <li>
                <div class="uk-navbar-right" uk-toggle="target: #offcanvas-flip">
                    <a class="uk-navbar-toggle" href="">
                        <span uk-icon="user"></span>
                        <span class="uk-margin-small-left">
                            <?= $user->last_name . ' ' . $user->first_name; ?>
                        </span>
                    </a>
                </div>
            </li>
        </ul>
    </div>
</nav>

<div id="offcanvas-nav-main-menu" uk-offcanvas="overlay: true">
    <div class="uk-offcanvas-bar uk-flex uk-flex-column"
         style="border-right: 2px solid #fffdfd;background-image: url(/./img/ladybug-1480102.jpg);">
        <ul class="uk-nav">
            <li><a class="main-menu-heder" href="#">
                    <span class="uk-margin-small-right"
                          uk-icon="icon: bookmark"></span><?= Yii::$app->translations['moduls'] ?>
                </a>
            </li>


            <li>
                <a class="main-menu-item" onclick="go('<?= Url::toRoute('/' . 'user'); ?>');">
                    <span class="uk-margin-small-right" uk-icon="icon: link"></span>
                    <?= Yii::$app->translations['user'] ?>
                </a>
            </li>
        </ul>
    </div>
</div>