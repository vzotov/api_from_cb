<?php
/*
 * id
 * name
 * placeholder
 * value
 * error
 * lable
 * required
 * rows
 * default
 */
if (@$required) {
    $requiredText = '<span class="uk-label uk-label-danger">( * )</span>';
}
if (!is_array(@$error)) {
    @$error = [];
}
if (!strlen(@$rows)) {
    $rows = 5;
}
?>
<div class="uk-margin">
    <label class="uk-form-label f-left" for="form-stacked-text" id="label-<?= @$id ?>"><?= @$lable ?>
        : <?= @$requiredText ?></label>
    <div class="uk-form-controls">
        <a class="uk-form-icon uk-form-icon-flip" style="background: #7ace7a;color: #fff;height: 34px;top: 30px;right: 2px;" onclick="default_<?=$id;?>();return false" uk-icon="icon: lifesaver"></a>
        <textarea class="uk-textarea"
                  style="padding-right: 45px;resize: none;"
                  id="<?= @$id ?>"
                  name="<?= @$name ?>"
                  rows="<?= @$rows ?>"
                  placeholder="<?= @$placeholder ?>"><?= @$value ?></textarea>
        <div class="uk-alert-danger" id="alert-danger-<?= @$id ?>" uk-alert style="margin-top: 2px;display: none">
            <a class="uk-alert-close" uk-close></a>
            <p id="alert-danger-text-<?= @$id ?>">
                <?php
                foreach (@$error as $key => $value) {
                    echo $value . '</br>';
                };
                ?>
            </p>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#<?= @$id ?>').on('focus', function () {
            $('#label-<?= @$id ?>').css('color', '#1e87f0');
        });
        $('#<?= @$id ?>').on('focusout', function () {
            $('#label-<?= @$id ?>').css('color', '#333');
        });
    })
    function default_<?=$id;?>() {
        $('#<?=@$id?>').val('');
        var default<?=$id;?> = ['<?=implode("','",$default)?>'];
        cout=0;
        while(default<?=$id;?>.length > cout){
            text = $('#<?=@$id?>').val();
            lineText = default<?=$id;?>[cout];
            $('#<?=@$id?>').val(text + lineText +"\n");
            cout++;
        }
    }
</script>


