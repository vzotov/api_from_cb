<?php
/*
 * id
 * name
 * placeholder
 * error
 * lable
 * required
 * openAction
 * model
 * atrName
 * atrID
 */

use yii\helpers\Url;

if (@$required) {
    $requiredText = '<span class="uk-label uk-label-danger">( * )</span>';
}
if (!is_array(@$error)) {
    @$error = [];
}
?>
<div class="uk-margin">
    <label class="uk-form-label f-left" for="<?= @$id ?>" id="label-<?= @$id ?>"><?= @$lable ?>
        : <?= @$requiredText ?></label>
    <div class="uk-form-controls">
        <div class="uk-inline" style="width: 100%">
            <a class="uk-form-icon uk-form-icon-flip" style="background: #7ace7a;color: #fff" onclick="openPopup<?=$id;?>();return false" uk-icon="icon: link"></a>
            <a class="uk-form-icon uk-form-icon" style="background: #bd4c4c;color: #fff" onclick="close_<?=$id;?>();return false" uk-icon="icon: close"></a>
            <input type="hidden" name="<?= @$name ?>" id="<?= @$id ?>" value="<?=@$model->$atrID?>">
            <input class="uk-input"
                   disabled
                   id="name-<?= @$id ?>"
                   type="text"
                   value="<?=@$model->$atrName?>"
                   style="padding-left: 47px !important;"
                   placeholder="<?= @$placeholder ?>">
        </div>
        <div class="uk-alert-danger" id="alert-danger-<?= @$id ?>" uk-alert style="margin-top: 2px;display: none">
            <a class="uk-alert-close" uk-close></a>
            <p id="alert-danger-text-<?= @$id ?>">
                <?php
                foreach (@$error as $key => $value) {
                    echo $value . '</br>';
                };
                ?>
            </p>
        </div>
    </div>
</div>
<script>

    $(document).ready(function () {
        $('#<?= @$id ?>').on('focus', function () {
            $('#label-<?= @$id ?>').css('color', '#1e87f0');
        });

        $('#<?= @$id ?>').on('focusout', function () {
            $('#label-<?= @$id ?>').css('color', '#333');
        });
    });

    function openPopup<?=$id;?>() {
        width = window.innerWidth/1.5;
        height = window.innerHeight;
        strWindowFeatures = 'location=yes,resizable=yes,scrollbars=yes,status=yes,width='+width+'px,height='+height+'px';
        newWin<?=$id;?> = window.open('<?=Url::toRoute(@$openAction. '?m=' . $id);?>',
            'WindowName<?= $id; ?>',
            strWindowFeatures
        );
        newWin<?=$id;?>.focus();

    }

    function close_<?=$id;?>() {
        $('#<?=@$id?>').val('');
        $('#name-<?=@$id?>').val('');
    }

    function select<?=$id?>(id,name) {
        $('#<?=@$id?>').val(id);
        $('#name-<?=@$id?>').val(name);
    }


</script>
