<?php
/**
 * id
 * value
 */
if (!strlen(@$value)) @$value = '<span class="uk-label uk-label-warning">'.Yii::$app->translations['no values'].'</span>';
$valArray = explode(' ',$value);
if (@$valArray[0]=='OK'){
    @$value = '<span class="uk-label uk-label-success">'.@$value.'</span>';
}elseif (!strlen(@$value) && @$valArray[0]!='OK'){
    @$value = '<span class="uk-label uk-label-danger">'.@$value.'</span>';
}
?>
<div id="<?= @$id ?>">
    <div class=""><?= @$value ?></div>
</div>
