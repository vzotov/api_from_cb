<?php
/*
 * id
 * name
 * placeholder
 * value
 * error
 * lable
 * option
 */
if (!count(@$option)) $option =[];
if (@$required) {
    $requiredText = '<span class="uk-label uk-label-danger">( * )</span>';
}
if (!is_array(@$error)) {
    @$error = [];
}
?>


<div class="uk-margin">
    <label class="uk-form-label f-left" for="form-stacked-select" id="label-<?= @$id ?>"><?= @$lable ?> : <?=@$requiredText?></label>
    <div class="uk-form-controls">
        <select class="uk-select" id="<?= @$id ?>" name="<?= @$name ?>">
            <?php
            foreach (@$option as $key => $val):
                if ((string)$key == $value):
                    ?>
                    <option value="<?= @$key ?>" selected><?= @$val ?></option>
                    <?php
                else:
                    ?>
                    <option value="<?= @$key ?>"><?= @$val ?></option>
                    <?php
                endif;
            endforeach;
            ?>
        </select>
        <div class="uk-alert-danger" id="alert-danger-<?= @$id ?>" uk-alert style="margin-top: 2px;display: none">
            <a class="uk-alert-close" uk-close></a>
            <p id="alert-danger-text-<?= @$id ?>">
                <?php
                foreach (@$error as $key => $value) {
                    echo $value . '</br>';
                };
                ?>
            </p>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#<?= @$id ?>').on('focus', function () {
            $('#label-<?= @$id ?>').css('color', '#1e87f0');
        });
        $('#<?= @$id ?>').on('focusout', function () {
            $('#label-<?= @$id ?>').css('color', '#333');
        });
    })
</script>


