<?php
/*
 * id
 * name
 * placeholder
 * value
 * error
 * lable
 * required
 * modul
 * action
 */

use yii\helpers\Url;

if (@$required) {
    $requiredText = '<span class="uk-label uk-label-danger">( * )</span>';
}
if (!is_array(@$error)) {
    @$error = [];
}
?>
<div class="uk-margin">
    <label class="uk-form-label f-left" for="<?= @$id ?>" id="label-<?= @$id ?>"><?= @$lable ?>
        : <?= @$requiredText ?></label>
    <div class="uk-form-controls">
        <div class="uk-inline" style="width: 100%">
            <a class="uk-form-icon uk-form-icon-flip" style="background: #7ace7a;color: #fff" onclick="upload_<?=$id;?>();return false" uk-icon="icon: upload"></a>
            <input class="uk-input"
                   id="<?= @$id ?>"
                   name="<?= @$name ?>"
                   type="text"
                   value="<?=@$value?>"
                   placeholder="<?= @$placeholder ?>">
        </div>
        <div class="uk-alert-danger" id="alert-danger-<?= @$id ?>" uk-alert style="margin-top: 2px;display: none">
            <a class="uk-alert-close" uk-close></a>
            <p id="alert-danger-text-<?= @$id ?>">
                <?php
                foreach (@$error as $key => $value) {
                    echo $value . '</br>';
                };
                ?>
            </p>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#<?= @$id ?>').on('focus', function () {
            $('#label-<?= @$id ?>').css('color', '#1e87f0');
        });

        $('#<?= @$id ?>').on('focusout', function () {
            $('#label-<?= @$id ?>').css('color', '#333');
        });

    });
    function upload_<?=$id;?>() {
        $.ajax({
            type: 'POST',
            url: '<?= Url::toRoute('/'.@$modul.'/default/'.@$action);?>',
            data: '<?= \Yii::$app->request->csrfParam ?>=<?= \Yii::$app->request->csrfToken ?>',
            dataType: 'json'
        }).done(function (json) {
            $('#<?= @$id ?>').val(json.value);
        }).fail(function () {
            console.log('fail');
        });
    }
</script>
