<?php
/*
 * modul
 * fromModul
 * action
 * lable
 * name
 * id
 * value
 */

use yii\helpers\Url;
use yii\widgets\Pjax;
?>
<div class="uk-margin">
    <div style="overflow:hidden">
    <label class="uk-form-label f-left" for="form-stacked-text" id="label-<?= @$id ?>"><?= @$lable ?>: <?= @$requiredText ?></label>
    </div>
    <div class="uk-placeholder" style="margin-top: 0px;position: relative;min-height: 120px">
        <div class="btn-group" style="position: absolute;right: 2px;top: 1px;">
            <button type="button" class="btn btn-success" onclick="openPopup<?= $id; ?>();"  uk-icon="list" style="background-color: #5cb85c;height: 34px;width: 40px"></button>
            <button type="button" class="btn btn-warning" uk-icon="close" style="background-color: #f0506e;height: 34px;width: 40px"></button>
        </div>
        <?php
        Pjax::begin(['id' =>'pjax-'.@$id, 'timeout' => 5000, 'enablePushState' => true, 'clientOptions' => ['method' => 'POST']]);
        ?>
        <table class="uk-table uk-table-hover uk-table-divider">
            <tbody>
            <tr>
                <td><input class="uk-checkbox" type="checkbox"></td>
                <td>Table Data</td>
                <td>Table Data</td>
                <td>Table Data</td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn" uk-icon="trash"></button>
                    </div>
                </td>
            </tr>
            <tr>
                <td><input class="uk-checkbox" type="checkbox"></td>
                <td>Table Data</td>
                <td>Table Data</td>
                <td>Table Data</td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn" uk-icon="trash"></button>
                    </div>
                </td>
            </tr>
            <tr>
                <td><input class="uk-checkbox" type="checkbox"></td>
                <td>Table Data</td>
                <td>Table Data</td>
                <td>Table Data</td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn" uk-icon="trash"></button>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
        <?php
        Pjax::end();
        ?>
    </div>
</div>
<script>
    function openPopup<?=$id;?>() {
        width = window.innerWidth/1.5;
        height = window.innerHeight;
        strWindowFeatures = 'location=yes,resizable=yes,scrollbars=yes,status=yes,width='+width+'px,height='+height+'px';
        newWin<?=$id;?> = window.open('<?=Url::toRoute('/' . @$modul . '/default/' . @$action . '?m=' . $id);?>',
            'WindowName<?= $id; ?>',
            strWindowFeatures
        );
        newWin<?=$id;?>.focus();

    }

   function <?=$id;?>(chec){
       console.log(chec);
       newWin<?=$id;?>.close();
       $.pjax({
           type       : 'POST',
           url        : '',
           container  : '#pjax-<?=@$id?>',
           data       : {},
           push       : true,
           replace    : false,
           timeout    : 10000,
           "scrollTo" : false
       });
    }
</script>