<?php
/**
 * @var $id
 * @var $value
 * @var $lable
 */
if (!strlen(@$value)) @$value = '<span class="uk-label uk-label-warning">'.Yii::$app->translations['no values'].'</span>';
?>
<div class="uk-flex uk-flex-center view-card-body" style="padding-left: 10px;" id="<?= @$id ?>">
    <div class="uk-width-1-1 uk-card uk-margin-left uk-text-emphasis uk-text-large">
        <div class="uk-margin">
            <h4 style="margin-bottom: 4px;"><?= @$lable ?>:</h4>
            <div class="uk-inline">
                <a class="uk-form-icon uk-form-icon-flip" onclick="copy<?=$id?>()" uk-icon="icon: copy"></a>
                <input class="uk-input uk-form-blank" id="copy<?=$id?>" type="text" value="<?= @$value ?>">
            </div>
        </div>
    </div>
</div>
<script>
    function copy<?=$id?>() {
        var copyText = document.getElementById("copy<?=$id?>");
        copyText.select();
        document.execCommand("copy");
        UIkit.notification({
                message: '<?=Yii::$app->translator('Copied').': '. @$lable ?>',
                pos: 'bottom-right',
                status: 'success'
            }
        )
    }
</script>

