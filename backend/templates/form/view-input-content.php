<?php
/**
 * id
 * lable
 * content
 */
if (!strlen(@$value)) @$value = '<span class="uk-label uk-label-warning">'.Yii::$app->translations['no values'].'</span>';
?>
<div class="uk-flex uk-flex-center view-card-body" style="padding-left: 10px;" id="<?= @$id ?>">
    <div class="uk-width-1-3 uk-card uk-card-small uk-text-bold"><h4><?= @$lable ?>:</h4></div>
    <div class="uk-width-1-1 uk-card uk-margin-left uk-text-emphasis uk-text-large"><?= @$content ?></div>
</div>
