<?php
/*
 * id
 * name
 * placeholder
 * value
 * error
 */
?>

<div class="uk-margin">
    <input class="uk-input"
           type="text"
           placeholder="<?= @$placeholder ?>"
           id="<?= @$id ?>"
           name="<?= @$name ?>"
           value="<?= @$value ?>">
    <?php
    if (count(@$error)):
        ?>
        <div class="uk-alert-danger" id="alert-danger-<?= @$id ?>" uk-alert style="margin-top: 2px;">
            <a class="uk-alert-close" uk-close></a>
            <p id="alert-danger-text-<?= @$id ?>">
                <?php
                foreach (@$error as $key => $value){
                echo $value.'</br>';
                };
                ?>
            </p>
        </div>
        <?php
    endif;
    ?>
</div>
