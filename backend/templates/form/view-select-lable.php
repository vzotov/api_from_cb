<?php
/**
 * id
 * value
 * lable
 * option
 */
?>
<div class="uk-flex uk-flex-center view-card-body" style="padding-left: 10px;" id="<?= @$id ?>">
    <div class="uk-width-1-3 uk-card uk-card-small uk-text-bold"><h4><?= @$lable ?>:</h4></div>
    <div class="uk-width-1-1 uk-card uk-margin-left uk-text-emphasis uk-text-large"><?= @$option[@$value]; ?></div>
</div>
