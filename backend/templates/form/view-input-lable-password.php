<?php
/**
 * modul
 * mvc
 * id
 * mid
 * lable
 */

use yii\helpers\Url;

?>
<div class="uk-flex uk-flex-center view-card-body" style="padding-left: 10px;" id="<?= @$id ?>">
    <div class="uk-width-1-3 uk-card uk-card-small uk-text-bold"><h4><?= @$lable ?>:</h4></div>
    <div class="uk-width-1-1 uk-card uk-margin-left uk-text-emphasis uk-text-large">
        <a href="#modal-<?= @$id ?>" uk-toggle><?= Yii::$app->translations['change'] ?></a>
    </div>
</div>


<div id="modal-<?= @$id ?>" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        <h4 class="uk-modal-title"><?= Yii::$app->translations['change password'] ?></h4>
        <form id="<?= @$id ?>_password_new" action="<?= Url::toRoute(@$mvc); ?>" method="post">
            <input id="form-token" type="hidden" name="<?= \Yii::$app->request->csrfParam ?>"
                   value="<?= \Yii::$app->request->csrfToken ?>"/>
            <input value="<?=@$mid?>" name="id" type="hidden">
            <?= Yii::$app->controller->renderPartial('@app/templates/form/input-lable-generator-password', [
                    'id' => 'password_new',
                    'name' => @$modul . '[password]',
                    'value' => '',
                    'placeholder' => '',
                    'required' => 1,
                    'lable' => Yii::$app->translations['new password'],
                    'error' => []
                ]
            );
            ?>
        </form>
        <p class="uk-text-right">
            <button class="uk-button uk-button-default uk-modal-close"
                    type="button"><?= Yii::$app->translations['action_close'] ?></button>
            <button class="uk-button uk-button-primary"
                    onclick="$('#<?= @$id ?>_password_new').submit();"
                    type="submit"><?= Yii::$app->translations['change'] ?></button>
        </p>
    </div>
</div>
<script>
    $(function () {
        $('#<?= @$id ?>_password_new').submit(function (e) {
            var $form = $(this);
            $.ajax({
                type: $form.attr('method'),
                url: $form.attr('action'),
                data: $form.serialize(),
                dataType: 'json'
            }).done(function (json) {
                $('.uk-alert-danger').html('');
                $('.uk-alert-danger').css('display', 'none');
                console.log(json);
                if (json.erros==0){
                    UIkit.modal('#modal-<?= @$id ?>').hide();
                    UIkit.notification({message: '<?= Yii::$app->translations['password changed'] ?>', status: 'success',pos: 'bottom-right'});
                }
                for (var item in json) {
                    <?= @$id ?>errors(item, json[item]);
                }
            }).fail(function () {
                console.log('fail');
            });
            e.preventDefault();
        });

        function <?= @$id ?>errors(fieldId, erorsValue) {
            $('#alert-danger-' + fieldId).html(erorsValue);
            $('#alert-danger-' + fieldId).css('display', 'block');
        }
    });
</script>