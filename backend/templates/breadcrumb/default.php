<?php

use yii\helpers\Url;

/**
 * modul
 * id
 */
?>
<ul class="uk-breadcrumb" style="float: left">
    <li><a href="<?= Url::toRoute('/' . @$modul); ?>"><?= \Yii::$app->translations[@$modul] ?></a></li>
    <?php
    if (is_int(@$id)):
        ?>
        <?
        if (Yii::$app->controller->action->id == 'view'):?>
            <li>
                <a class="uk-disabled" href=""><?= \Yii::$app->translations['view'] ?></a>
            </li>
        <? else: ?>
            <li>
                <a href="<?= Url::toRoute('/' . @$modul . '/default/view?id=' . @$id); ?>"><?= \Yii::$app->translations['view'] ?></a>
            </li>
            <li class="uk-disabled"><a
                        href="<?= Url::toRoute('/' . @$modul . '/default/view?id=' . @$id); ?>"><?= \Yii::$app->translations['update'] ?></a>
            </li>
        <?endif; ?>
        <?php
    else:
        ?>
        <li class="uk-disabled"><a><?= \Yii::$app->translations['creature'] ?></a></li>
        <?php
    endif;
    ?>
</ul>
