<?php
/**
 * subpanels
 * model
 */

?>
<div class="uk-grid-match uk-child-width-expand@s uk-flex-center" style="margin-top: 15px">
    <div uk-filter="target: .js-filter">
        <div class="uk-grid-small uk-grid-divider uk-child-width-auto" uk-grid>
            <div>
                <ul class="uk-subnav uk-subnav-pill" uk-margin>
                    <li uk-filter-control class="uk-active">
                        <a href="#"><?= Yii::$app->translations['all']; ?></a>
                    </li>
                </ul>
            </div>
            <div>
                <ul class="uk-subnav uk-subnav-pill" uk-margin>
                    <?php
                    foreach (@$subpanels as $key => $value):
                    ?>
                    <li uk-filter-control="[data-tags='<?=@$value['group']?>']">
                        <a href="#">#<?= Yii::$app->translations[@$value['moduls']]; ?></a>
                    </li>
                    <?php
                    endforeach;
                    ?>
                </ul>
            </div>
        </div>

        <ul class="js-filter uk-child-width-1-1 uk-child-width-1-1@m" uk-grid="masonry: true">
            <?php
            foreach (@$subpanels as $key => $value):
                ?>
                <li data-tags="<?= @$value['group'] ?>" data-size="large">
                    <?= Yii::$app->controller->renderPartial('@app/moduls/' . $value['moduls'] . '/views/subpanels/' . $value['name'],
                        [
                            'subpanels' => $value,
                            'model' => @$model,
                        ]
                    );
                    ?>
                </li>
            <?php
            endforeach;
            ?>
        </ul>
    </div>
</div>
