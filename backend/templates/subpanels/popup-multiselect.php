<?php
/**
 * @var $subpanels
 * @var $model
 */

use yii\helpers\Url;
use yii\widgets\Pjax;
?>
<div class="uk-margin">
        <div class="btn-group" style="float: right">
            <?php
            $openActionName = $subpanels['openActionName'];
            if (Yii::$app->permissionCheck($subpanels['moduls'], $openActionName) || Yii::$app->getUser()->identity->is_admin == '1'):
            ?>
            <button type="button" class="btn btn-success"  onclick="openPopup<?= @$subpanels['id']; ?>();" style=""><?= \Yii::$app->translations['select'] ?></button>
            <?php
            else:
            ?>
                <button type="button" class="btn btn-success"  disabled style=""><?= \Yii::$app->translations['select'] ?></button>
            <?php
            endif;
            ?>
        </div>
</div>
<script>
    function openPopup<?= @$subpanels['id']; ?>() {
        width = window.innerWidth/1.5;
        height = window.innerHeight;
        strWindowFeatures = 'location=yes,resizable=yes,scrollbars=yes,status=yes,width='+width+'px,height='+height+'px';
        var newWin<?=@$subpanels['id'];?> = window.open('<?=Url::toRoute(@$subpanels['openAction'] . '?m=' . @$subpanels['id']);?>',
            'WindowName<?= @$subpanels['id']; ?>',
            strWindowFeatures
        );
        newWin<?=@$subpanels['id'];?>.focus();

    }

    function save<?=@$subpanels['id'];?>(chec) {
        <?php
        $modelId = @$subpanels['relation']['id'];
        ?>
        var formData = {
            '<?= \Yii::$app->request->csrfParam ?>': '<?= \Yii::$app->request->csrfToken ?>',
            'select':chec,
            'id':'<?=@$model->$modelId;?>'
        };
        $.ajax({
            type: 'POST',
            url: '<?=Url::toRoute(@$subpanels['saveAction']);?>',
            data:formData,
        }).done(function (data) {
            go('<?=$_SERVER['REDIRECT_URL']?>?<?=$_SERVER['REDIRECT_QUERY_STRING']?>');
        }).fail(function () {
            console.log('fail');
        });
    }
</script>