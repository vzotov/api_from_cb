<?php
/**
 * user
 */

use yii\helpers\Url;

?>
<div id="offcanvas-flip" uk-offcanvas="flip: true; overlay: true">

    <div class="uk-offcanvas-bar"
         style="border-left: 2px solid #fffdfd;background-image: url(/./img/ladybug-1480102.jpg);">

        <button class="uk-offcanvas-close color-fff" type="button" uk-close></button>
        <div uk-icon="icon: user; ratio: 4;color:#fff" class="uk-flex uk-flex-center color-fff"></div>
        <div class="uk-flex uk-flex-center color-fff">
            <h3><?= $user->last_name . ' ' . $user->first_name . ' ' . $user->middle_name; ?></h3>
        </div>
        <div class="" uk-margin>
            <button class="uk-button uk-button-default" onclick="$('#form-logout').submit()" ><?= Yii::$app->translations['exit'] ?></button>
                    </div>

    </div>
</div>

<form action="<?= Url::toRoute('/api/logout'); ?>" id="form-logout" method="post">
    <input id="form-token" type="hidden" name="<?= \Yii::$app->request->csrfParam ?>"
           value="<?= \Yii::$app->request->csrfToken ?>"/>
</form>