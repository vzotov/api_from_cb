<?php
/**
 * @var $modul
 * @var $id
 */

use yii\helpers\Url;
use backend\extension\pbxUrlReturn;
/*
 * формируем параметры url для возврата на форму связанного модуля
 */
$returnParams = pbxUrlReturn::returnUrl();

/*
 * Url для кнопки отмена
 */
$returnUrl = pbxUrlReturn::getUrl();

/*
 * Повторяем параметры возврата на форму связанного модуля
 */
$repeatParams = pbxUrlReturn::repeatRParams();
?>
<div class="btn-group" style="float: right;position: relative;top: 10px;">
    <?php
    if (Yii::$app->getUser()->identity->is_admin == '1' || Yii::$app->permissionCheck($modul, 'Update')):
    ?>
    <a type="submit" href="<?= Url::toRoute('/' . @$modul . '/default/update?id=' . @$id.$repeatParams); ?>"
       class="btn btn-success"><?= Yii::$app->translations['action_edit'] ?></a>
    <?php
    else:
    ?>
        <a type="submit" class="btn btn-success" disabled=""><?= Yii::$app->translations['action_edit'] ?></a>
    <?php
    endif;
    ?>
    <a type="button" href="<?= Url::toRoute($returnUrl); ?>"
       class="btn btn-warning"><?= Yii::$app->translations['action_close'] ?></a>
</div>
