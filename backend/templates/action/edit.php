<?php
/**
 * @var $modul
 * @var $return_action
 */

use yii\helpers\Url;

?>
<div class="edit-block-action">
    <div class="btn-group" style="float: right">
        <?php
        if (Yii::$app->getUser()->identity->is_admin == '1' || Yii::$app->permissionCheck($modul, 'Save')):
        ?>
        <button type="submit" class="btn btn-success" ><?= \Yii::$app->translations['action_save'] ?></button>
        <?php
        else:
        ?>
            <button type="submit" class="btn btn-success" disabled><?= \Yii::$app->translations['action_save'] ?></button>
        <?php
        endif;
        ?>
        <a type="button" href="<?php echo Url::toRoute(@$return_action); ?>"
           class="btn btn-warning"><?= \Yii::$app->translations['action_close'] ?></a>
    </div>
</div>
