<?php
/**
 * @var $modul
 */

use yii\helpers\Url;
use backend\extension\pbxUrlReturn;
/*
 * формируем параметры url для возврата на форму связанного модуля
 */
$returnParams = pbxUrlReturn::returnUrl('?');

/*
 * Url для кнопки отмена
 */
$returnUrl = pbxUrlReturn::getUrl();


if (Yii::$app->getUser()->identity->is_admin == '1' || Yii::$app->permissionCheck($modul, 'Save')):
    ?>
    <a type="submit" href="<?=Url::toRoute( '/'.$modul.'/default/update'.$returnParams)?>" class="btn btn-success">
        <?= \Yii::$app->translations['create'] ?>
    </a>
<?php
else:
    ?>
    <a type="submit" class="btn btn-success" disabled="">
        <?= \Yii::$app->translations['create'] ?>
    </a>
<?php
endif;
?>
