<?php
/**
 * @var $modul
 * @var $id
 */

use yii\helpers\Url;
use backend\extension\pbxUrlReturn;
/*
 * формируем параметры url для возврата на форму связанного модуля
 */
$returnParams = pbxUrlReturn::returnUrl();

/*
 * Url для кнопки отмена
 */
$returnUrl = pbxUrlReturn::getUrl();

/*
 * Повторяем параметры возврата на форму связанного модуля
 */
$repeatParams = pbxUrlReturn::repeatRParams();
?>
<div class="btn-group" style="float: right;position: relative;top: 10px;">
    <a type="button" href="<?= Url::toRoute($returnUrl); ?>"
       class="btn btn-warning"><?= Yii::$app->translations['action_close'] ?></a>
</div>
