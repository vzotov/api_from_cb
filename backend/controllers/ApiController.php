<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\BaseJson;
use yii\db\Query;
use common\models\LoginForm;


/**
 * Pbx controller
 */
class ApiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login','authentication', 'error','cbr'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','cbr'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        //$this->layout = 'error';
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout' => 'error',
            ],
        ];
    }


    public function actionCbr(){
        $this->enableCsrfValidation = false;
        $date = Yii::$app->request->get('date');
        $query = new Query();
        $valcurs = $query->select('*')->from('valcurs')->where(['valcurs_date'=>$date])->all();
        Yii::$app->response->statusCode = 200;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $baseJson = new BaseJson();
        $loginData['LoginForm'] = [
            'username'=> Yii::$app->request->get('username'),
            'password'=> Yii::$app->request->get('password'),
        ];
        $model = new LoginForm();
        if ($model->load($loginData) && $model->login()) {
            return $valcurs;
        }else{
            return [
                'action' => 'Authentication',
                'mes' => 'is bad',
            ];
        }

    }

    public function actionAuthentication(){
        $this->enableCsrfValidation = false;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = new LoginForm();
        $loginData['LoginForm'] = [
            'username'=> Yii::$app->request->get('username'),
            'password'=> Yii::$app->request->get('password'),
        ];
        if ($model->load($loginData) && $model->login()) {
            return [
                'action' => 'Authentication',
                'mes' => 'is ok',
                'session'=> Yii::$app->session->getId(),
            ];
        }else{
            return [
                'action' => 'Authentication',
                'mes' => 'is bad',
            ];
        }
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
           // return $this->goHome();
        }
        $this->layout = 'login';
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }



    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
