<?php

use app\models\User;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

$modulName = 'user';
$session = \Yii::$app->session;
if (strlen($session['spUser'])) {
    $search = unserialize($session['spUser']);
} else {
    $search = array();
};
$params = \Yii::$app->params;
?>
<div class="block-interface z-depth-1">
    <form id="<?= $modulName ?>-search" action="<?php echo Url::toRoute('/' . $modulName . '/default/search'); ?>"
          method="post">
        <input id="form-token" type="hidden" name="<?= \Yii::$app->request->csrfParam ?>"/>
        <div class="modul-card-badge modul-label"><?= Yii::$app->translations[$modulName]; ?></div>
        <div uk-filter="target: .js-filter" style="overflow: hidden;padding-right: 8px;">

            <div class="uk-grid-small uk-grid-divider uk-child-width-auto" uk-grid>
                <div>
                    <ul class="uk-subnav uk-subnav-pill" uk-margin>
                        <li class="uk-active" uk-filter-control="[data-tags='main']">
                            <a href="#" class="uk-active"><?= Yii::$app->translations['main']; ?></a>
                        </li>
                    </ul>
                </div>
                <div>
                    <ul class="uk-subnav uk-subnav-pill" uk-margin>
                        <li uk-filter-control><a href="#"><?= Yii::$app->translations['all']; ?></a></li>
                    </ul>
                </div>
            </div>
            <ul class="js-filter uk-child-width-1-2 uk-child-width-1-3@m uk-text-center" uk-grid="masonry: true">
                <script>
                    $(document).ready(function () {
                        $('.js-filter').removeAttr( 'style' );
                    });
                </script>
                <li data-tags="main" class="ls-search-form" style="margin-top: 0;">
                    <?= Yii::$app->controller->renderPartial('@app/templates/form/input-lable', [
                            'id' => 'username',
                            'name' => 'user[username]',
                            'value' => @$search['username'],
                            'placeholder' => '',
                            'lable' => Yii::$app->translations['username'],
                            'error' => @$model->errors['username']
                        ]
                    );
                    ?>
                </li>
                <li data-tags="main" class="ls-search-form" style="margin-top: 0;">
                    <?= Yii::$app->controller->renderPartial('@app/templates/form/input-lable', [
                            'id' => 'role',
                            'name' => 'user[role]',
                            'value' => @$search['role'],
                            'placeholder' => '',
                            'lable' => Yii::$app->translations['role'],
                            'error' => @$model->errors['role']
                        ]
                    );
                    ?>
                </li>
                <li data-tags="all" class="ls-search-form" style="margin-top: 0;">
                    <?= Yii::$app->controller->renderPartial('@app/templates/form/input-lable', [
                            'id' => 'first_name',
                            'name' => 'user[first_name]',
                            'value' => @$search['first_name'],
                            'placeholder' => '',
                            'lable' => Yii::$app->translations['first_name'],
                            'error' => @$model->errors['first_name']
                        ]
                    );
                    ?>
                </li>
                <li data-tags="main" class="ls-search-form" style="margin-top: 0;">
                    <?= Yii::$app->controller->renderPartial('@app/templates/form/input-lable', [
                            'id' => 'last_name',
                            'name' => 'user[last_name]',
                            'value' => @$search['last_name'],
                            'placeholder' => '',
                            'lable' => Yii::$app->translations['last_name'],
                            'error' => @$model->errors['last_name']
                        ]
                    );
                    ?>
                </li>
                <li data-tags="all" data-size="medium" style="margin-top: 0;">
                    <?= Yii::$app->controller->renderPartial('@app/templates/form/input-lable', [
                            'id' => 'middle_name',
                            'name' => 'user[middle_name]',
                            'value' => @$search['middle_name'],
                            'placeholder' => '',
                            'lable' => Yii::$app->translations['middle_name'],
                            'error' => @$model->errors['middle_name']
                        ]
                    );
                    ?>
                </li>
                <li data-tags="all" data-size="medium" style="margin-top: 0;">
                    <?= Yii::$app->controller->renderPartial('@app/templates/form/input-lable', [
                            'id' => 'email',
                            'name' => 'user[email]',
                            'value' => @$search['email'],
                            'placeholder' => '',
                            'lable' => Yii::$app->translations['email'],
                            'error' => @$model->errors['email']
                        ]
                    );
                    ?>
                </li>
                <li data-tags="all" data-size="medium" style="margin-top: 0;">
                    <?= Yii::$app->controller->renderPartial('@app/templates/form/select-lable', [
                            'id' => 'status',
                            'name' => 'user[status]',
                            'value' => @$search['status'],
                            'lable' => Yii::$app->translations['status'],
                            'error' => @$model->errors['status'],
                            'option' => Yii::$app->translations['cb_user_status'],
                        ]
                    );
                    ?>
                </li>
                <li data-tags="all" data-size="medium" style="margin-top: 0;">
                    <?= Yii::$app->controller->renderPartial('@app/templates/form/select-lable', [
                            'id' => 'user_type',
                            'name' => 'user[user_type]',
                            'value' => @$search['user_type'],
                            'lable' => Yii::$app->translations['user_type'],
                            'error' => @$model->errors['user_type'],
                            'option' => Yii::$app->translations['cb_user_type'],
                        ]
                    );
                    ?>
                </li>
            </ul>
            <div class="btn-group" style="padding: 10px 10px 10px 0;">
                <a type="submit" href="/<?= $modulName; ?>/default/update" class="btn btn-success">
                    <?= \Yii::$app->translations['create'] ?>
                </a>
            </div>
            <div class="btn-group" style="float: right;padding: 10px">
                <button type="button" onclick="clear_search();"
                        class="btn btn-primary"><?= Yii::$app->translations['clear']; ?></button>
                <button type="submit" class="btn btn-success"><?= \Yii::$app->translations['find']; ?></button>
            </div>
        </div>
    </form>

    <?php
    $query = User::find();
    if (is_array($search)) {
        foreach (@$search as $key => $value) {
            if ($key == 'status' && strlen($value)) {
                $query->andWhere(['=', $key, $value]);
            } elseif (strlen($value)) {
                $query->andWhere(['like', $key, $value]);
            }

        }
    }

    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'key' => 'id',
        'pagination' => [
            'pageSize' => 50,
        ],
    ]);

    Pjax::begin(['id' => $modulName . 'pjax', 'timeout' => 5000, 'enablePushState' => true, 'clientOptions' => ['method' => 'POST']]);
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'id' => 'list_' . $modulName,
        'columns' => [
            'username',
            'first_name',
            'last_name',
            'list_ations' => array(
                'format' => 'raw',
                'options' => [
                    'style' => 'width: 150px;',
                ],
                'value' => function ($data) {
                    return Yii::$app->controller->renderPartial('@app/templates/list_actions/index',
                        array('data' => $data,
                            'modul' => 'user',
                            'controller' => 'default',
                            'key' => 'id',
                            'id' => 'user',
                            'options' => array(),
                        )
                    );
                }
            ),
        ],
    ]);
    Pjax::end();
    ?>
    <script type="text/javascript">

        function reload_<?=$modulName;?>_list() {
            jQuery.pjax.defaults.timeout = 5000;
            jQuery.pjax.reload({container: '#<?=$modulName;?>pjax'});
        }

        function delete_<?=$modulName;?>(id) {
            $.ajax({
                type: 'get',
                url: "<?php echo Url::toRoute('/' . $modulName . '/default/delete');?>",
                data: 'id=' + id
            }).done(function (data) {
                reload_<?=$modulName;?>_list();
                console.log(data);
            }).fail(function () {
                console.log('fail');
            });
        }

        function activity_<?=$modulName;?>(id) {
            $.ajax({
                type: 'get',
                url: "<?php echo Url::toRoute('/' . $modulName . '/default/activity');?>",
                data: 'id=' + id
            }).done(function (data) {
                reload_<?=$modulName;?>_list();
                console.log(data);
            }).fail(function () {
                console.log('fail');
            });
        }


        function searchClear_groups_list() {
            $('.search-field').val('');
        }


        $(function () {
            $('#<?=$modulName?>-search').submit(function (e) {
                var $form = $(this);
                $.ajax({
                    type: $form.attr('method'),
                    url: $form.attr('action'),
                    data: $form.serialize()
                }).done(function (data) {
                    reload_<?=$modulName;?>_list();
                    console.log(data);
                }).fail(function () {
                    console.log('fail');
                });
                e.preventDefault();
            });
        });


        function clear_search() {
            $('.uk-select').val('');
            $('.uk-input').val('');
            $.ajax({
                type: 'get',
                url: "<?php echo Url::toRoute('/' . $modulName . '/default/searchclear');?>",
                data: 'id='
            }).done(function (data) {
                reload_<?=$modulName;?>_list();
                console.log(data);
            }).fail(function () {
                console.log('fail');
            });
        }
    </script>
