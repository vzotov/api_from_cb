<?php
/*
 * user
 *
 */

use yii\helpers\Url;
use yii\widgets\Pjax;
use backend\extension\pbxUrlReturn;

/*
 * названия модуля
 */
$modulName = 'user';

/*
 * url для возврата к форме на которую нужно вернутся
 */
$returnUrl = pbxUrlReturn::getUrl();

?>
<script>
    $(function () {
        $('#user_edit').submit(function (e) {
            var $form = $(this);
            $.ajax({
                type: $form.attr('method'),
                url: $form.attr('action'),
                data: $form.serialize(),
                dataType: 'json'
            }).done(function (json) {
                if (json.erros == 0) {
                    go('<?= Url::toRoute($returnUrl);?>');
                }

                $('.uk-alert-danger').html('');
                $('.uk-alert-danger').css('display','none');
                console.log(json);
                for (var item in json) {
                    errors(item, json[item]);
                }
            }).fail(function () {
                console.log('fail');
            });
            e.preventDefault();
        });
        function errors(fieldId, erorsValue) {
            $('#alert-danger-' + fieldId).html(erorsValue);
            $('#alert-danger-' + fieldId).css('display','block');
        }
    });
</script>
<div class="uk-card uk-card-default panel-num-1">
<form id="user_edit" action="<?= Url::toRoute('/user/default/save');?>" method="post">
    <input name="id" type="hidden" value="<?= @$user->id; ?>">
    <input id="form-token" type="hidden" name="<?= \Yii::$app->request->csrfParam ?>"
           value="<?= \Yii::$app->request->csrfToken ?>"/>
    <?= Yii::$app->controller->renderPartial('@app/templates/breadcrumb/default', ['modul'=>$modulName,'id'=>@$user->id]);?>
    <?= Yii::$app->controller->renderPartial('@app/templates/action/edit', ['return_action' => @$returnUrl]); ?>
    <div class="uk-grid-match uk-child-width-expand@s uk-text-center uk-flex-center">
        <div uk-filter="target: .js-filter" style="overflow: hidden">
            <script>
                $(document).ready(function () {
                    $('.js-filter').removeAttr( 'style' );
                });
            </script>
            <div class="uk-grid-small uk-grid-divider uk-child-width-auto" uk-grid>
                <div>
                    <ul class="uk-subnav uk-subnav-pill" uk-margin>
                        <li class="uk-active" class="uk-active" uk-filter-control="[data-tags*='main-fields']">
                            <a href="#"><?= Yii::$app->translations['main-fields']; ?></a>
                        </li>
                    </ul>
                </div>
                <div>
                    <ul class="uk-subnav uk-subnav-pill" uk-margin>
                        <li uk-filter-control="[data-tags*='required-fields']">
                            <a href="#"><?= Yii::$app->translations['required-fields']; ?>
                                <span class="uk-label uk-label-danger">( * )</span></a>
                        </li>
                        <li uk-filter-control>
                            <a href="#"><?= Yii::$app->translations['all-filds']; ?></a>
                        </li>
                    </ul>
                </div>
            </div>

            <ul class="js-filter uk-child-width-1-1 uk-child-width-1-2@m uk-text-center" uk-grid="masonry: true">
                <li data-tags="required-fields main-fields" class="ls-search-form" style="margin-top: 0;">
                    <?= Yii::$app->controller->renderPartial('@app/templates/form/input-lable', [
                            'id' => 'username',
                            'name' => 'user[username]',
                            'value' => @$user->username,
                            'placeholder' => '',
                            'required'=>1,
                            'lable' => Yii::$app->translations['username'],
                            'error' => @$model->errors['username']
                        ]
                    );
                    ?>
                </li>
                <li data-tags="all-filds" class="ls-search-form" style="margin-top: 0;">
                    <?= Yii::$app->controller->renderPartial('@app/templates/form/input-lable', [
                            'id' => 'extension',
                            'name' => 'user[extension]',
                            'value' => @$user->extension,
                            'placeholder' => '',
                            'lable' => Yii::$app->translations['extension'],
                            'error' => @$model->errors['extension']
                        ]
                    );
                    ?>
                </li>
                <li data-tags="<?if(!is_int(@$user->id))echo 'required-fields';?> main-fields" class="ls-search-form" style="margin-top: 0;">
                    <?= Yii::$app->controller->renderPartial('@app/templates/form/input-lable-generator-password', [
                            'id' => 'password_hash',
                            'name' => 'user[password]',
                            'value' => '',
                            'placeholder' => '',
                            'required'=>!is_int(@$user->id),
                            'lable' => Yii::$app->translations['password'],
                            'error' => @$model->errors['password']
                        ]
                    );
                    ?>
                </li>
                <li data-tags="all-filds main-fields" class="ls-search-form" style="margin-top: 0;">
                    <?= Yii::$app->controller->renderPartial('@app/templates/form/input-lable', [
                            'id' => 'first_name',
                            'name' => 'user[first_name]',
                            'value' => @$user->first_name,
                            'placeholder' => '',
                            'lable' => Yii::$app->translations['first_name'],
                            'error' => @$model->errors['first_name']
                        ]
                    );
                    ?>
                </li>
                <li data-tags="required-fields main-fields" class="ls-search-form" style="margin-top: 0;">
                    <?= Yii::$app->controller->renderPartial('@app/templates/form/input-lable', [
                            'id' => 'last_name',
                            'name' => 'user[last_name]',
                            'value' => @$user->last_name,
                            'placeholder' => '',
                            'required'=>1,
                            'lable' => Yii::$app->translations['last_name'],
                            'error' => @$model->errors['last_name']
                        ]
                    );
                    ?>
                </li>
                <li data-tags="all-filds" data-size="medium" style="margin-top: 0;">
                    <?= Yii::$app->controller->renderPartial('@app/templates/form/input-lable', [
                            'id' => 'middle_name',
                            'name' => 'user[middle_name]',
                            'value' => @$user->middle_name,
                            'placeholder' => '',
                            'lable' => Yii::$app->translations['middle_name'],
                            'error' => @$model->errors['middle_name']
                        ]
                    );
                    ?>
                </li>
                <li data-tags="all-filds" data-size="medium" style="margin-top: 0;">
                    <?= Yii::$app->controller->renderPartial('@app/templates/form/input-lable', [
                            'id' => 'email',
                            'name' => 'user[email]',
                            'value' => @$user->email,
                            'placeholder' => '',
                            'lable' => Yii::$app->translations['email'],
                            'error' => @$model->errors['email']
                        ]
                    );
                    ?>
                </li>
                <li data-tags="required-fields main-fields" data-size="medium" style="margin-top: 0;">
                    <?= Yii::$app->controller->renderPartial('@app/templates/form/select-lable', [
                            'id' => 'status',
                            'name' => 'user[status]',
                            'required'=>1,
                            'value' => @$user->user_activity,
                            'lable' => Yii::$app->translations['user_activity'],
                            'error' => @$model->errors['status'],
                            'option' => Yii::$app->translations['cb_user_status'],
                        ]
                    );
                    ?>
                </li>
                <li data-tags="required-fields main-fields" data-size="medium" style="margin-top: 0;">
                    <?= Yii::$app->controller->renderPartial('@app/templates/form/popup-lable-select', [
                            'id' => 'role',
                            'name' => 'user[role]',
                            'placeholder' => '',
                            'required' => 1,
                            'lable' => Yii::$app->translations['role'],
                            'openAction'=>'/role/default/select',
                            'model'=>app\models\Role::findOne(@$user->role),
                            'atrName'=>'role_name',
                            'atrID'=>'role_id',
                        ]
                    );
                    ?>
                </li>
                <li data-tags="required-fields main-fields" data-size="medium" style="margin-top: 0;">
                    <?php
                    foreach (Yii::$app->params['api_ats_config'] as $key => $value) {
                        $pbx[$key] = Yii::$app->translations[$key];
                    }
                    echo Yii::$app->controller->renderPartial('@app/templates/form/select-lable', [
                            'id' => 'telephony_server',
                            'name' => 'user[telephony_server]',
                            'value' => @$user->telephony_server,
                            'required'=>1,
                            'lable' => Yii::$app->translations['telephony_server'],
                            'error' => @$model->errors['telephony_server'],
                            'option' => $pbx,
                        ]
                    );
                    ?>
                </li>
                <li data-tags="required-fields main-fields" data-size="medium" style="margin-top: 0;">
                    <?= Yii::$app->controller->renderPartial('@app/templates/form/select-lable', [
                            'id' => 'is_admin',
                            'name' => 'user[is_admin]',
                            'required'=>0,
                            'value' => @$user->is_admin,
                            'lable' => Yii::$app->translations['is_admin'],
                            'error' => @$model->errors['is_admin'],
                            'option' => Yii::$app->translations['cb_is_admin'],
                        ]
                    );
                    ?>
                </li>
            </ul>
        </div>
    </div>
    <?= Yii::$app->controller->renderPartial('@app/templates/action/edit', ['return_action' => @$returnUrl]); ?>
</form>
</div>