<?php
/*
 * user
 *
 */

$modulName = 'user';
?>
    <input name="id" type="hidden" value="<?= @$user->id; ?>">
    <input id="form-token" type="hidden" name="<?= \Yii::$app->request->csrfParam ?>"
           value="<?= \Yii::$app->request->csrfToken ?>"/>
<?= Yii::$app->controller->renderPartial('@app/templates/breadcrumb/default', ['modul' => $modulName, 'id' => @$user->id]); ?>
<?= Yii::$app->controller->renderPartial('@app/templates/action/view', ['modul' => $modulName, 'id' => @$user->id]); ?>
    <div class="uk-grid-match uk-child-width-expand@s uk-flex-center">
        <div uk-filter="target: .js-filter">

            <div class="uk-grid-small uk-grid-divider uk-child-width-auto" uk-grid>
                <div>
                    <ul class="uk-subnav uk-subnav-pill" uk-margin>
                        <li uk-filter-control class="uk-active">
                            <a href="#"><?= Yii::$app->translations['all']; ?></a>
                        </li>
                    </ul>
                </div>
                <div>
                    <ul class="uk-subnav uk-subnav-pill" uk-margin>
                        <li uk-filter-control="[data-tags*='user']">
                            <a href="#">#user</a>
                        </li>
                        <li uk-filter-control="[data-tags*='role']">
                            <a href="#">#role</a>
                        </li>
                    </ul>
                </div>
            </div>

            <ul class="js-filter uk-child-width-1-1 uk-child-width-1-2@m" uk-grid="masonry: true">
                <li data-tags="user" data-size="large">
                    <div class="uk-card uk-card-default uk-card-body uk-card-hover">
                        <div class="uk-card-badge uk-label">#user</div>
                        <h3 class="uk-card-title"><?=Yii::$app->translations['user values']?></h3>
                        <?= Yii::$app->controller->renderPartial('@app/templates/form/view-input-lable', [
                                'id' => 'last_name',
                                'value' => @$user->last_name,
                                'lable' => Yii::$app->translations['last_name'],
                            ]
                        );
                        ?>
                        <?= Yii::$app->controller->renderPartial('@app/templates/form/view-input-lable', [
                                'id' => 'first_name',
                                'value' => @$user->first_name,
                                'lable' => Yii::$app->translations['first_name'],
                            ]
                        );
                        ?>
                        <?= Yii::$app->controller->renderPartial('@app/templates/form/view-input-lable', [
                                'id' => 'middle_name',
                                'value' => @$user->middle_name,
                                'lable' => Yii::$app->translations['middle_name'],
                            ]
                        );
                        ?>
                        <?= Yii::$app->controller->renderPartial('@app/templates/form/view-input-lable', [
                                'id' => 'username',
                                'value' => @$user->username,
                                'lable' => Yii::$app->translations['username'],
                            ]
                        );
                        ?>
                        <?= Yii::$app->controller->renderPartial('@app/templates/form/view-input-lable-password', [
                                'id' => 'password',
                                'mid' => @$user->id,
                                'lable' => Yii::$app->translations['password'],
                                'mvc' => '/user/default/chpassword',
                                'modul' => 'user',
                            ]
                        );
                        ?>
                    </div>
                </li>
                <li data-tags="user" data-size="large">
                    <div class="uk-card uk-card-default uk-card-body uk-card-hover">
                        <div class="uk-card-badge uk-label">#user</div>
                        <h3 class="uk-card-title"><?=Yii::$app->translations['accesses']?></h3>
                        <?= Yii::$app->controller->renderPartial('@app/templates/form/view-input-lable', [
                                'id' => 'extension',
                                'value' => @$user->extension,
                                'lable' => Yii::$app->translations['extension'],
                            ]
                        );
                        ?>
                        <?php
                        foreach (Yii::$app->params['api_ats_config'] as $key => $value) {
                            $pbx[$key] = Yii::$app->translations[$key];
                        }
                        echo Yii::$app->controller->renderPartial('@app/templates/form/view-select-lable', [
                                'id' => 'telephony_server',
                                'value' => @$user->telephony_server,
                                'lable' => Yii::$app->translations['telephony_server'],
                                'option' => $pbx,
                            ]
                        );
                        ?>
                        <?= Yii::$app->controller->renderPartial('@app/templates/form/view-input-lable', [
                                'id' => 'email',
                                'value' => @$user->email,
                                'lable' => Yii::$app->translations['email'],
                            ]
                        );
                        ?>
                        <?= Yii::$app->controller->renderPartial('@app/templates/form/view-select-lable', [
                                'id' => 'status',
                                'value' => @$user->status,
                                'lable' => Yii::$app->translations['status'],
                                'option' => Yii::$app->translations['cb_user_status'],
                            ]
                        );
                        ?>
                        <?= Yii::$app->controller->renderPartial('@app/templates/form/view-select-lable', [
                                'id' => 'is_admin',
                                'value' => @$user->is_admin,
                                'lable' => Yii::$app->translations['is_admin'],
                                'option' => Yii::$app->translations['cb_is_admin'],
                            ]
                        );
                        ?>
                    </div>
                </li>
                <li data-tags="role" data-size="large" class="uk-width-1-1">
                    <div class="uk-card uk-card-default uk-card-body uk-card-hover">
                        <div class="uk-card-badge uk-label">#role</div>
                        <h3 class="uk-card-title"><?=Yii::$app->translations['role']?></h3>
                        <table class="uk-table uk-table-hover uk-table-divider">
                            <thead>
                            <tr>
                                <th>Table Heading</th>
                                <th>Table Heading</th>
                                <th>Table Heading</th>
                                <th>Table Heading</th>
                                <th>Table Heading</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Table Data</td>
                                <td>Table Data</td>
                                <td>Table Data</td>
                                <td>Table Data</td>
                                <td>Table Data</td>
                            </tr>
                            <tr>
                                <td>Table Data</td>
                                <td>Table Data</td>
                                <td>Table Data</td>
                                <td>Table Data</td>
                                <td>Table Data</td>
                            </tr>
                            <tr>
                                <td>Table Data</td>
                                <td>Table Data</td>
                                <td>Table Data</td>
                                <td>Table Data</td>
                                <td>Table Data</td>
                            </tr>
                            <tr>
                                <td>Table Data</td>
                                <td>Table Data</td>
                                <td>Table Data</td>
                                <td>Table Data</td>
                                <td>Table Data</td>
                            </tr>
                            <tr>
                                <td>Table Data</td>
                                <td>Table Data</td>
                                <td>Table Data</td>
                                <td>Table Data</td>
                                <td>Table Data</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </li>
            </ul>
        </div>
    </div>
<?= Yii::$app->controller->renderPartial('@app/templates/action/view', ['modul' => $modulName, 'return_action' => @$return_action]); ?>