<?php

namespace app\moduls\user\controllers;

use app\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

/**
 * Default controller for the `user` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index page for the module
     * @return string
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $sp = unserialize($session['spCalls']);
        $query = User::find();
        if (is_array($sp)){
            foreach (@$sp as $key=>$value){
                if (strlen($value)) $query->andWhere(['like', $key, $value]);
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'key' => 'id',
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'params' => Yii::$app->params,
            'translations' => Yii::$app->translations,
        ]);
    }
    /**
     * Renders the view page for the module
     * @return string
     */
    public function actionView()
    {
        $user = new User;
        if (array_key_exists('id',$_REQUEST)){
            $user = User::find()->where(['id'=>$_REQUEST['id']])->one();
        }

        return $this->render('view',
            [
                'user'=>$user,
            ]
        );
    }
    /**
     * Renders the edit page for the module
     * @return string
     */
    public function actionEdit()
    {
        $get = Yii::$app->request->get();
        if(array_key_exists('id', $get) && $get['id'] != '') {
            $model = User::findOne($get['id']);
        } else {
            $model = new User;
        }

        if(!strlen($model->id)) {
            $return_action = 'index';
        } else {
            $return_action = 'view/?id=' . $model->id;
        }

        return $this->render('edit',[
            'user' => $model,
            'return_action' => $return_action,
        ]);
    }
    /**
     * Renders the update page for the module
     * @return string
     */
    public function actionUpdate()
    {
        $get = Yii::$app->request->get();
        if(array_key_exists('id', $get) && $get['id'] != '') {
            $model = User::findOne($get['id']);
        } else {
            $model = new User;
        }

        if(!strlen($model->id)) {
            $return_action = 'index';
        } else {
            $return_action = 'view/?id=' . $model->id;
        }

        return $this->render('edit',[
            'user' => $model,
            'return_action' => $return_action,
        ]);
    }
    /**
     * Save action for app\models\User model
     * @return string
     */
    public function actionSave()
    {
        $post = Yii::$app->request->post();
        if ($post && $post['id'] != '') {
            $model = User::findOne($post['id']);
            $model->updated_at = time();
        } else {
            $model = new User;
            $model->created_at = time();
            $model->updated_at = 0;
            $model->auth_key = '1';
            $model->password_reset_token = '1';
            //$model->status = 10;
            //$model->role = 0;
        }

        if($post['user']['password'] != '') {
            $model->password_hash = password_hash($post['user']['password'], PASSWORD_DEFAULT);
        }

        $model->username = $post['user']['username'];
        $model->email = $post['user']['email'];
        $model->user_activity = $post['user']['status'];
        $model->status = $post['user']['status'];
        $model->extension = $post['user']['extension'];
        $model->telephony_server = $post['user']['telephony_server'];
        $model->last_name =  $post['user']['last_name'];
        $model->middle_name = $post['user']['middle_name'];
        $model->first_name =  $post['user']['first_name'];
        $model->first_name =  $post['user']['first_name'];
        $model->role = $post['user']['role'];
        $model->is_admin = $post['user']['is_admin'];
        $model->save();
        $erros = [];
        if(is_array($model->errors)) {
            foreach ($model->errors as $key => $value){
                $erros[$key] = $value[0]."\n";
            }
        }

        if(!count($erros)) {
            $return = [
                'key' => $model->id,
                'erros' => '0',
            ];
        } else {
            $return = $erros;
        }
        return json_encode($return);
    }
    /**
     * Delete row action for app\models\User model
     * @return void
     */
    public function actionDelete(){
        $get = Yii::$app->request->get();
        if (array_key_exists('id', $get) && $get['id'] != ''){
            $model = User::find()->where(['id' => $get['id']])->one();
            if ($model->id!=1){
                $model->delete();
            }

        }
    }
    /**
     * @return void
     */
    public function actionActivity(){

        if (array_key_exists('id',$_REQUEST)){
            $model = User::find()->where(['id'=>$_REQUEST['id']])->one();
            if ($model->user_activity =='1' && $model->id!=1){
                $model->user_activity ='0';
                $model->status ='0';
            }else{
                $model->user_activity = '1';
                $model->status ='10';
            }
            $model->save();
        }
    }
    /**
     * @inheritdoc
     */
    public function actionSearch()
    {
        $session = \Yii::$app->session;
        $session['spUser'] = serialize($_REQUEST['user']);
    }

    public function actionChpassword()
    {
        $model = User::find()->where(['id' => $_REQUEST['id']])->one();
        $erros = [];
        if ($_REQUEST['user']['password'] != '' && Yii::$app->user->identity->is_admin) {
            $model->password_hash = password_hash($_REQUEST['user']['password'], PASSWORD_DEFAULT);
            $model->save();
        }elseif (Yii::$app->user->id == $model->id){
            $model->password_hash = password_hash($_REQUEST['user']['password'], PASSWORD_DEFAULT);
            $model->save();
        }else{
            $erros['password_new'] = Yii::$app->translations['password is required']."\n";
        }

        if(!count($erros)) {
            $return = [
                'key' => $model->id,
                'erros' => '0',
            ];
        } else {
            $return = $erros;
        }
        return json_encode($return);
    }

    public function actionSearchclear()
    {
        $session = \Yii::$app->session;
        $session['spUser'] = '';
    }

}
