<?php
/**
 * required parameters
 * moduleName
 * actions
 * class
 */
return [
    'moduleName' => 'user',
    'actions' => [],
    'class' => 'app\moduls\user\user',
    'settings' => [
        'version' => '2.01',
        'teg' => 'admin',
        'description' => 'Модуль для управления пользователями в системе'
    ],
];