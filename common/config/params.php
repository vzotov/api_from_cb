<?php
return [
   /* 'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',*/
    'user.passwordResetTokenExpire' => 3600,
    'install'=>[
        'mkdir'=>[
            '/etc/asterisk/sip',
            '/etc/asterisk/sip/peer',
            '/etc/asterisk/sip/register',
        ],
        'default'=>[
            '/etc/asterisk/sip/peer/default.conf',
            '/etc/asterisk/sip/register/default.conf'
        ]
    ]
];
