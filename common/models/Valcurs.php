<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "valcurs".
 *
 * @property integer $valcurs_id
 * @property string $valcurs_valute_id
 * @property string $valcurs_num_code
 * @property string $valcurs_char_code
 * @property string $valcurs_nominal
 * @property string $valcurs_name
 * @property string $valcurs_value
 * @property string $valcurs_date
 */
class Valcurs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'valcurs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['valcurs_valute_id', 'valcurs_num_code', 'valcurs_char_code', 'valcurs_nominal', 'valcurs_name', 'valcurs_date'], 'required'],
            [['valcurs_date'], 'safe'],
            [['valcurs_valute_id'], 'string', 'max' => 8],
            [['valcurs_num_code'], 'string', 'max' => 6],
            [['valcurs_char_code'], 'string', 'max' => 4],
            [['valcurs_nominal'], 'string', 'max' => 255],
            [['valcurs_name', 'valcurs_value'], 'string', 'max' => 50],
            [['valcurs_valute_id', 'valcurs_date'], 'unique', 'targetAttribute' => ['valcurs_valute_id', 'valcurs_date']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'valcurs_id' => @Yii::$app->translations['valcurs_id'],
            'valcurs_valute_id' => @Yii::$app->translations['valcurs_valute_id'],
            'valcurs_num_code' => @Yii::$app->translations['valcurs_num_code'],
            'valcurs_char_code' => @Yii::$app->translations['valcurs_char_code'],
            'valcurs_nominal' => @Yii::$app->translations['valcurs_nominal'],
            'valcurs_name' => @Yii::$app->translations['valcurs_name'],
            'valcurs_value' => @Yii::$app->translations['valcurs_value'],
            'valcurs_date' => @Yii::$app->translations['valcurs_date'],
        ];
    }
}
