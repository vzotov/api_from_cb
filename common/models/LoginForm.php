<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }
        
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::$app->translations['id'],
            'username' => Yii::$app->translations['username'],
            'auth_key' => Yii::$app->translations['auth_key'],
            'password_hash' => Yii::$app->translations['password_hash'],
            'password_reset_token' => Yii::$app->translations['password_reset_token'],
            'email' => Yii::$app->translations['email'],
            'status' => Yii::$app->translations['status'],
            'role' => Yii::$app->translations['role'],
            'created_at' => Yii::$app->translations['created_at'],
            'updated_at' => Yii::$app->translations['updated_at'],
            'telephony_server' => Yii::$app->translations['telephony_server'],
            'extension' => Yii::$app->translations['extension'],
            'user_activity' => Yii::$app->translations['user_activity'],
            'accesses_moduls' => Yii::$app->translations['accesses_moduls'],
            'user_type' => Yii::$app->translations['user_type'],
            'endpoint_id' => Yii::$app->translations['endpoint_id'],
            'password' => Yii::$app->translations['password'],
        ];
    }
}
