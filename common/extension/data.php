<?php
/**
 * Created by PhpStorm.
 * User: vzotov
 * Date: 17.02.18
 * Time: 18:09
 */
namespace common\extension;
class data
{
    static function login($mes,$params =''){
        $mesDc = print_r($mes);
        date_default_timezone_set('UTC');
        $file = 'amievent.log';//\Yii::$app->params['log_fails'];
        $current = date("Y-m-d H:i:s").' '.$mesDc.' '.$params.' ( '.time().' )'."\n";
        file_put_contents($file, $current, FILE_APPEND | LOCK_EX);
    }

    static function GUID()
    {
        if (function_exists('com_create_guid') === true) {
            return trim(com_create_guid(), '{}');
        }

        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X',
            mt_rand(0, 65535),
            mt_rand(0, 65535),
            mt_rand(0, 65535),
            mt_rand(16384, 20479),
            mt_rand(32768, 49151),
            mt_rand(0, 65535),
            mt_rand(0, 65535),
            mt_rand(0, 65535)
        );
    }
}