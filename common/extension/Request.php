<?php
/**
 * Created by PhpStorm.
 * User: vzotov
 * Date: 09.04.18
 * Time: 15:36
 */
namespace common\extension;

use function GuzzleHttp\Psr7\str;

class Request
{
    static function ruRequest($url,$content){
        if (strlen($url)){
            return file_get_contents($url, false, stream_context_create(array(
                'http' => array(
                    'method'  => 'POST',
                    'header'  => 'Content-type: application/x-www-form-urlencoded',
                    'content' => 'json_base64='.base64_encode(json_encode($content)),
                )
            )));
        }else{
            return '';
        }
}

    public function arrayToGetParams($content=array()){
        foreach ($content as $key=>$value){
            if ($value!=''){
                $params[] = $key.'='.$value;
            }
        }
        return implode('&',$params);
}

    public function toGetParams($content=array()){
        $params = array();
        foreach ($content['key'] as $key=>$value){
            if (strlen($value)){
                $params[] = $value.'='.$content['value'][$key];
            }
        }
        return implode('&',$params);
}



    public function sendPostData($url, $post){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Authorization: Basic TkFWSUdhjljkipFWSUdBTlRASU5ESUFCVUxMUy5DT00='
            )
        );
        $result = curl_exec($ch);
        curl_close($ch);  // Seems like good practice
        return $result;
    }
}