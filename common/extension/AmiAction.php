<?php

namespace common\extension;
use app\moduls\endpoint\endpoint;

class AmiAction
{
    public function click2call()
    {
        $ami = \Yii::$app->ami;
        $strCallerId = "Click2Call <2006>";
        $oSocket = fsockopen($ami['host'], $ami['port'], $errnum, $errdesc) or die("Connection to host failed");
        fputs($oSocket, "Action: login\r\n");
        fputs($oSocket, "Events: off\r\n");
        fputs($oSocket, "Username: " . $ami['user'] . "\r\n");
        fputs($oSocket, "Secret: " . $ami['secret'] . "\r\n\r\n");
        fputs($oSocket, "Action: originate\r\n");
        fputs($oSocket, "Channel: PJSIP/" . "\r\n");
        fputs($oSocket, "WaitTime: 30\r\n");
        fputs($oSocket, "CallerId: $strCallerId\r\n");
        fputs($oSocket, "Exten: " ."89105301118". "\r\n");
        fputs($oSocket, "Context: out-1\r\n");
        fputs($oSocket, "Priority: 1\r\n\r\n");
        fputs($oSocket, "Action: Logoff\r\n\r\n");
        sleep(1);
        fclose($oSocket);
        echo 'ok';
    }

    public function redirect($ext,$Channel,$extraChannel)
    {
        $ami = \Yii::$app->ami;
        date_default_timezone_set('UTC');
        $teme = time();
        $socket = fsockopen($ami['host'], $ami['port'], $errnum, $errdesc) or die("Connection to host failed");
        fputs($socket, "Action: Login\r\n");
        fputs($socket, "UserName: " . $ami['user']  . "\r\n");
        fputs($socket, "Secret: " . $ami['secret'] . "\r\n\r\n");
        fputs($socket, "Action: Redirect\r\n");
        fputs($socket, "Channel: $extraChannel\r\n");
        fputs($socket, "ExtraChannel: $Channel\r\n");
        fputs($socket, "Exten: $ext\r\n");
        fputs($socket, "Context: out-1\r\n");
        fputs($socket, "Priority: 1\r\n\r\n");
        fputs($socket, "Action: Logoff\r\n\r\n");
        $wrets = '';
        while (!feof($socket)) {
            $wrets .= fgets($socket, 8192);
        }
        fclose($socket);
        $lineArray = explode("\r\n",$wrets);
        echo '<pre>',print_r($lineArray),'<pre>';
    }

    static function localCalls($endpoint)
    {
        $ami = \Yii::$app->ami;
        $ext = \Yii::$app->getUser()->identity->extension;
        $last = \Yii::$app->getUser()->identity->last_name;
        $first = \Yii::$app->getUser()->identity->first_name;
        $strCallerId = "$last $first <$ext>";
        $oSocket = fsockopen($ami['host'], $ami['port'], $errnum, $errdesc) or die("Connection to host failed");
        fputs($oSocket, "Action: login\r\n");
        fputs($oSocket, "Events: off\r\n");
        fputs($oSocket, "Username: " . $ami['user'] . "\r\n");
        fputs($oSocket, "Secret: " . $ami['secret'] . "\r\n\r\n");
        fputs($oSocket, "Action: originate\r\n");
        fputs($oSocket, "Channel: PJSIP/$ext" . "\r\n");
        fputs($oSocket, "WaitTime: 30\r\n");
        fputs($oSocket, "CallerId: $strCallerId\r\n");
        fputs($oSocket, "Exten: " ."#$endpoint". "\r\n");
        fputs($oSocket, "Context: out-1\r\n");
        fputs($oSocket, "Priority: 1\r\n\r\n");
        fputs($oSocket, "Action: Logoff\r\n\r\n");
        sleep(1);
        fclose($oSocket);
        echo 'ok';
    }

    static function ChanSpy($endpoint)
    {
        $ami = \Yii::$app->ami;
        $ext = \Yii::$app->getUser()->identity->extension;
        $strCallerId = "$endpoint <$ext>";
        $oSocket = fsockopen($ami['host'], $ami['port'], $errnum, $errdesc) or die("Connection to host failed");
        fputs($oSocket, "Action: login\r\n");
        fputs($oSocket, "Events: off\r\n");
        fputs($oSocket, "Username: " . $ami['user'] . "\r\n");
        fputs($oSocket, "Secret: " . $ami['secret'] . "\r\n\r\n");
        fputs($oSocket, "Action: originate\r\n");
        fputs($oSocket, "Channel: PJSIP/$ext" . "\r\n");
        fputs($oSocket, "WaitTime: 30\r\n");
        fputs($oSocket, "CallerId: $strCallerId\r\n");
        fputs($oSocket, "Exten: " ."@$endpoint". "\r\n");
        fputs($oSocket, "Context: out-1\r\n");
        fputs($oSocket, "Priority: 1\r\n\r\n");
        fputs($oSocket, "Action: Logoff\r\n\r\n");
        sleep(1);
        fclose($oSocket);
        echo 'ok';
    }

    static function Hangup($channel)
    {
        $ami = \Yii::$app->ami;
        $oSocket = fsockopen($ami['host'], $ami['port'], $errnum, $errdesc) or die("Connection to host failed");
        fputs($oSocket, "Action: login\r\n");
        fputs($oSocket, "Events: off\r\n");
        fputs($oSocket, "Username: " . $ami['user'] . "\r\n");
        fputs($oSocket, "Secret: " . $ami['secret'] . "\r\n\r\n");
        fputs($oSocket, "Action: Hangup\r\n");
        fputs($oSocket, "Channel: $channel\r\n\r\n");
        fputs($oSocket, "Action: Logoff\r\n\r\n");
        sleep(1);
        fclose($oSocket);
        echo 'ok';
    }

    static function PJSIPShowEndpoints()
    {
        $ami = \Yii::$app->ami;
        date_default_timezone_set('UTC');
        $teme = time();
        $socket = fsockopen($ami['host'], $ami['port'], $errnum, $errdesc) or die("Connection to host failed");
        fputs($socket, "Action: Login\r\n");
        fputs($socket, "UserName: " . $ami['user']  . "\r\n");
        fputs($socket, "Secret: " . $ami['secret'] . "\r\n\r\n");
        fputs($socket, "Action: PJSIPShowEndpoints\r\n\r\n");
        fputs($socket, "Action: Logoff\r\n\r\n");
        $count = 0;
        $array = array();
        $wrets = '';
        while (!feof($socket)) {
            $wrets .= fgets($socket, 8192);
        }
        fclose($socket);
        $lineArray = explode("\r\n",$wrets);
        foreach ($lineArray as $value){
            $pName = @explode(': ',$value)[0];
            $pVal = @explode(': ',$value)[1];
            if (@$pName == 'Event'){
                $count++;
            }
            $event[$count][$pName] = $pVal;
        }

        foreach ($event as $value){
            if (@$value['Event']=='EndpointList'){
                $endpoint[$value['ObjectName']] = $value;
            }
        }
        return @$endpoint;
    }

    static function CoreShowChannels()
    {
        $ami = \Yii::$app->ami;
        date_default_timezone_set('UTC');
        $teme = time();
        $socket = fsockopen($ami['host'], $ami['port'], $errnum, $errdesc) or die("Connection to host failed");
        fputs($socket, "Action: Login\r\n");
        fputs($socket, "UserName: " . $ami['user']  . "\r\n");
        fputs($socket, "Secret: " . $ami['secret'] . "\r\n\r\n");
        fputs($socket, "Action: CoreShowChannels\r\n\r\n");
        fputs($socket, "Action: Logoff\r\n\r\n");
        $count = 0;
        $array = array();
        $wrets = '';
        while (!feof($socket)) {
            $wrets .= fgets($socket, 8192);
        }
        fclose($socket);
        $lineArray = explode("\r\n",$wrets);
        foreach ($lineArray as $value){
            $pName = @explode(': ',$value)[0];
            $pVal = @explode(': ',$value)[1];
            if (@$pName == 'Event'){
                $count++;
            }
            $event[$count][$pName] = $pVal;
        }
        $channel = [];
        foreach ($event as $value){
            if (@$value['Event']=='CoreShowChannel'){
                $channel[$value['Uniqueid']] = $value;
            }
        }
        return $channel;
    }
}