<?php

namespace common\extension;

use common\models\Calls;
use common\models\In;
use common\models\Out;
use common\models\Sip;
use common\models\CallsEvent;
use common\models\Endpoint;
use backend\extension\Rquest;

class AmiEvent
{
    public function Event($mes)
    {
        $eventName = $mes['Event'];

        switch ($eventName) {
            case 'Newchannel':
                $this->Newchannel($mes);
                break;
            case 'DialBegin':
                $this->DialBegin($mes);
                break;
            case 'BridgeEnter':
                $this->BridgeEnter($mes);
                break;
            case 'HangupRequest':
                $this->HangupRequest($mes);
                break;
            case 'Hangup':
                $this->Hangup($mes);
                break;
            case 'SoftHangupRequest':
                $this->SoftHangupRequest($mes);
                break;
            case 'VarSet':
                $this->VarSet($mes);
                break;
            case 'DeviceStateChange':
                $this->DeviceStateChange($mes);
                break;
        }
    }

    public function CallCheck($mes)
    {
        $callsEvent = CallsEvent::find()->where(['calls_event_uniqueid' => @$mes['Linkedid']])->one();
        $callCheck = Calls::find()->where(['calls_id' => @$callsEvent->calls_event_calls_id])->one();
        return $callCheck;
    }

    public function AddCallsEvent($callsID,$mes){
        date_default_timezone_set('UTC');
        $callsEvent = new CallsEvent();
        $callsEvent->calls_event_calls_id = $callsID;
        $callsEvent->calls_event_date = date("Y-m-d H:i:s");
        $callsEvent->calls_event_name = $mes['Event'];
        $callsEvent->calls_event_uniqueid = $mes['Uniqueid'];
        $callsEvent->calls_event_linkedid = $mes['Linkedid'];
        $callsEvent->calls_event_text = json_encode($mes);
        $callsEvent->save();
    }

    public function VarSet($mes)
    {
        if ($mes['Variable'] == 'MIXMONITOR_FILENAME') {
            $callCheck = $this->CallCheck($mes);
            $callCheck->calls_record = $mes['Value'];
            $callCheck->save();
            $this->AddCallsEvent($callCheck->calls_id,$mes);
        }
    }
    public function DeviceStateChange($mes)
    {
        $wsMesArray = [
            'action'=>'extension',
            'message'=>[
                'events' =>'DeviceStateChange',
            ]
        ];
        Rquest::sendRquest('http://test.local',$wsMesArray);
    }
    static function ChannelName($channel)
    {
        $a = explode('/', $channel);
        return explode('-', $a[1])[0];
    }

    public function NewChannel($mes)
    {
        date_default_timezone_set('UTC');
        $callCheck = $this->CallCheck($mes);
        $wsMesArray = [
            'action'=>'channels',
            'message'=>[
                'events' =>'NewChannel',
            ]
        ];
        Rquest::sendRquest('http://test.local',$wsMesArray);
        /*
         * обычный звонок
         */
        if (!is_object($callCheck)) {
            $channel = $this->ChannelName($mes['Channel']);
            $sip = Sip::find()->where(['sip_name' => $channel])->one();
            if (is_object($sip)) {
                $calls = new Calls;
                $calls->calls_status = $mes['ChannelStateDesc'];
                $calls->calls_date_beginning = date("Y-m-d H:i:s");
                $calls->calls_phone = $mes['CallerIDNum'];
                $calls->calls_extension = $mes['Exten'] . '(did)';
                $calls->calls_directions = 'in';
                $calls->calls_dial_type = 'usual';
                $calls->calls_uniqueid = $mes['Uniqueid'];
                $calls->calls_linkedid = $mes['Linkedid'];
                $calls->calls_trunk = $channel;
                $calls->save();
                $this->AddCallsEvent($calls->calls_id,$mes);
                return $calls->calls_id;
            }
            $endpoint = Endpoint::find()->where(['endpoint_aors' => $channel])->one();
            if (is_object($endpoint)) {
                $calls = new Calls;
                $calls->calls_status = $mes['ChannelStateDesc'];
                $calls->calls_date_beginning = date("Y-m-d H:i:s");
                $calls->calls_phone = $mes['Exten'];
                $calls->calls_extension = $mes['CallerIDNum'];
                $calls->calls_directions = 'out';
                $calls->calls_dial_type = 'usual';
                $calls->calls_uniqueid = $mes['Uniqueid'];
                $calls->calls_linkedid = $mes['Linkedid'];
                $calls->save();
                $this->AddCallsEvent($calls->calls_id,$mes);
                return $calls->calls_id;
            }

        } else {
            /*
             * если такое уже есть
             */
        }

    }

    public function DialBegin($mes)
    {
        date_default_timezone_set('UTC');
        $callCheck = $this->CallCheck($mes);
        if (is_object($callCheck)) {
            $this->AddCallsEvent($callCheck->calls_id,$mes);
            $channel = $this->ChannelName($mes['Channel']);
            /*
            $sip= Sip::find()->where(['sip_name'=>$channel])->one();
            if (is_object($sip)){

            }
            */
            $endpoint = Endpoint::find()->where(['endpoint_aors' => $channel])->one();
            if (is_object($endpoint)) {
                $callCheck->calls_trunk = $this->ChannelName($mes['DestChannel']);
                $callCheck->calls_phone = $mes['Exten'];
                $callCheck->save();
                return $callCheck->calls_id;
            }

        } else {
            /*
             * если такое пока нет
             */
        }

    }

    public function HangupRequest($mes)
    {
        $callCheck = $this->CallCheck($mes);
        if (is_object($callCheck)) {
            $this->AddCallsEvent($callCheck->calls_id,$mes);
            $channel = $this->ChannelName($mes['Channel']);

            $in = In::find()->where(['in_name' => $mes['Context']])->one();
            if (is_object($in)) {
                $callCheck->calls_hangup_request = $channel;
                $callCheck->calls_hangup_request_label = $mes['ChannelStateDesc'].' (client)';
                $callCheck->save();
                return $callCheck->calls_id;
            }

            $out = Out::find()->where(['out_name' => $mes['Context']])->one();
            if (is_object($out)) {
                $callCheck->calls_hangup_request = $channel;
                $callCheck->calls_hangup_request_label = $mes['ChannelStateDesc'].' (operator)';
                $callCheck->save();
                return $callCheck->calls_id;
            }

        }
    }

    public function SoftHangupRequest($mes){
        //echo print_r($mes);
    }

    public function Hangup($mes)
    {
        date_default_timezone_set('UTC');
        $callCheck = $this->CallCheck($mes);
        /*
         * заплатка
         */
        if (@$callCheck->calls_status == 'Hangup'){
            return 0;
        }
        if (is_object($callCheck)) {
            $this->AddCallsEvent($callCheck->calls_id,$mes);
            $channel = $this->ChannelName($mes['Channel']);
            $sip = Sip::find()->where(['sip_name' => $channel])->one();
            if (is_object($sip)) {
                $wsMesArray = [
                    'action'=>'channels',
                    'message'=>[
                        'events' =>'Hangup',
                    ]
                ];
                Rquest::sendRquest('http://test.local',$wsMesArray);

                $callCheck->calls_date_end = date("Y-m-d H:i:s");
                $callCheck->calls_status = 'Hangup';
                $callCheck->calls_durability = (int)strtotime($callCheck->calls_date_end) - (int)strtotime($callCheck->calls_date_beginning);
                if (strlen($callCheck->calls_date_answer))
                    $callCheck->calls_billable = (int)strtotime($callCheck->calls_date_end) - (int)strtotime($callCheck->calls_date_answer);
                $callCheck->calls_hangup_cause = $mes['Cause'];
                $callCheck->calls_hangup_cause_txt = $mes['Cause-txt'];
                $callCheck->save();
                return $callCheck->calls_id;
            }
            if (!$mes['ConnectedLineNum'] == '<unknown>'){
                $endpoint = Endpoint::find()->where(['endpoint_aors' => $mes['ConnectedLineNum']])->one();
            }else{
                $endpoint = Endpoint::find()->where(['endpoint_aors' => $channel])->one();
            }
            if (is_object($endpoint)) {
                $wsMesArray = [
                    'action'=>'channels',
                    'message'=>[
                        'events' =>'Hangup',
                    ]
                ];
                Rquest::sendRquest('http://test.local',$wsMesArray);

                $callCheck->calls_date_end = date("Y-m-d H:i:s");
                $callCheck->calls_status = 'Hangup';
                $callCheck->calls_durability = (int)strtotime($callCheck->calls_date_end) - (int)strtotime($callCheck->calls_date_beginning);
                if (strlen($callCheck->calls_date_answer))
                    $callCheck->calls_billable = (int)strtotime($callCheck->calls_date_end) - (int)strtotime($callCheck->calls_date_answer);
                $callCheck->calls_hangup_cause = $mes['Cause'];
                $callCheck->calls_hangup_cause_txt = $mes['Cause-txt'];
                $callCheck->save();
                return $callCheck->calls_id;
            }
        }
    }

    public function BridgeEnter($mes)
    {
        date_default_timezone_set('UTC');
        $callCheck = $this->CallCheck($mes);
        if (is_object($callCheck)) {
            $this->AddCallsEvent($callCheck->calls_id,$mes);
            $channel = $this->ChannelName($mes['Channel']);
            $wsMesArray = [
                'action'=>'channels',
                'message'=>[
                    'events' =>'BridgeEnter',
                ]
            ];
            Rquest::sendRquest('http://test.local',$wsMesArray);

            /*
            $sip= Sip::find()->where(['sip_name'=>$channel])->one();
            if (is_object($sip)){

            }
            */
            $sip = Sip::find()->where(['sip_name' => $channel])->one();
            $out = Out::find()->where(['out_name' => $mes['Context']])->one();
            if (is_object($sip) && is_object($out)) {
                $callCheck->calls_date_answer = date("Y-m-d H:i:s");
                $callCheck->calls_status = 'BridgeEnter';
                $callCheck->save();
                return $callCheck->calls_id;
            }
            $in = In::find()->where(['in_name' => $mes['Context']])->one();
            if (is_object($sip) && is_object($in)) {
                $callCheck->calls_date_answer = date("Y-m-d H:i:s");
                $callCheck->calls_status = 'BridgeEnter';
                $callCheck->calls_extension = $mes['ConnectedLineNum'];
                $callCheck->save();
                return $callCheck->calls_id;
            }
        } else {
            /*
             * если такое пока нет
             */
        }
    }
}

?>
