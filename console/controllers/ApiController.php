<?php

namespace console\controllers;

use yii\console\Controller;
use common\models\Valcurs;

class ApiController extends Controller
{
    public function actionIndex()
    {
        echo "hello world\n";
    }

    public function actionCbr(){
        $xml = simplexml_load_file('http://www.cbr.ru/scripts/XML_daily.asp','SimpleXMLElement', LIBXML_NOCDATA);
        $xmlToArray = json_decode(json_encode($xml),true);
        foreach ($xmlToArray['Valute'] as $key => $value){
            $valcurs = new Valcurs();
            $valcurs->valcurs_valute_id = $value['@attributes']['ID'];
            $valcurs->valcurs_num_code = $value['NumCode'];
            $valcurs->valcurs_char_code = $value['CharCode'];
            $valcurs->valcurs_nominal = $value['Nominal'];
            $valcurs->valcurs_name = $value['Name'];
            $valcurs->valcurs_value  = $value['Value'];
            $valcurs->valcurs_date = date("Y-m-d");
            $valcurs->save();
        }
    }

}