<?php

namespace console\extension;

use yii\console\Application;

class pbxConsole extends Application
{
    public $translations = [];
    public $mConfigDir = '';
    public $mConfig = '';
    public $mModulsDir = 'moduls';
    public $mConf;
    public $ami;
    public $white_ip;

    public function init()
    {
        parent::init();
    }

    public function pbxDateTimeGTM(){
        date_default_timezone_set('UTC');
        return \date("Y-m-d H:i:s");
    }
}