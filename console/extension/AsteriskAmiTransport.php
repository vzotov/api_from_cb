<?php

namespace console\extension;

abstract class AsteriskAmiTransport
{
    private $_amiSocket;
    private $_inBuffer;
    private $_outBuffer;
    private $_messages;
    private $_logEnabled;
    private $_logFile;
    public $_conf;

    function __construct($config = array(), $logEnabled = true)
    {
        $this->_conf = $config;
        $this->_config = $this->_conf;
        $this->_messages = array();
        $this->resetOutBuffer();
        $this->resetInBuffer();
        $this->activateSession();
        if (!$logEnabled) {
            $this->deactivateLog();
        }
        $this->connect();
    }

    private final function connect()
    {

        $this->printMessage('Trying to open asterisk socket...');
        while (!$this->_amiSocket) {
            $this->_amiSocket = \socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
            if ($this->_amiSocket === false) {
                $errorCode = \socket_last_error();
                $errorMsg = \socket_strerror($errorCode);
                $this->printError(sprintf('Socket creation failed [%s]: %s', $errorCode, $errorMsg));
                sleep(1);
            } else {
                $this->printMessage('Socket created.');
                while (!\socket_connect($this->_amiSocket, $this->_config['host'], $this->_config['port'])) {
                    $errorCode = \socket_last_error();
                    $errorMsg = \socket_strerror($errorCode);
                    $this->printError(sprintf('Socket connection failed [%s]: %s', $errorCode, $errorMsg));
                    sleep(1);
                }
                $this->printMessage('Socket connected.');
            }
        }
        \socket_set_option($this->_amiSocket, SOL_SOCKET, SO_RCVBUF, 1000000);
        $this->login();
    }

    private final function reconnect()
    {
        if ($this->_amiSocket) {
            \socket_close($this->_amiSocket);
            $this->_amiSocket = null;
        }
        $this->connect();
    }

    private final function login()
    {
        $asteriskUserString = sprintf("Username: %s\r\n", $this->_config['user']);
        $asteriskSecretString = sprintf("Secret: %s\r\n", $this->_config['secret']);
        $this->addToOutBuffer("Action: Login\r\n");
        $this->addToOutBuffer($asteriskUserString);
        $this->addToOutBuffer($asteriskSecretString);
        $this->addToOutBuffer("\r\n");
        $this->write();
    }

    protected final function activateSession()
    {
        $this->_sessionActivated = true;
    }

    protected final function deactivateSession()
    {
        $this->_sessionActivated = false;
    }

    private final function isSessionActive()
    {
        return $this->_sessionActivated;
    }

    protected final function setLogFile($fileName)
    {
        if ($this->_logEnabled) {
            $file = fopen($fileName, 'a+');
            $this->_logFile = $file;
        }
    }

    protected final function activateLog()
    {
        $this->_logEnabled = true;
    }

    protected final function deactivateLog()
    {
        $this->_logEnabled = false;
    }

    protected function printError($message)
    {
        if ($this->_logEnabled) {
            fwrite(($this->_logFile) ? $this->_logFile : STDERR, sprintf("[%s] Error: %s\n", date('Y-m-d H:i:s'), $message));
        }
    }

    protected function printMessage($message)
    {
        echo print_r($message."\n");
        if ($this->_logEnabled) {
            //fwrite(($this->_logFile) ? $this->_logFile : STDERR, sprintf("[%s] Message: %s\n", date('Y-m-d H:i:s'), $message));
        }
    }

    protected function printNotice($message)
    {
        if ($this->_logEnabled) {
            //fwrite(($this->_logFile) ? $this->_logFile : STDERR, sprintf("[%s] Notice: %s\n", date('Y-m-d H:i:s'), $message));
        }
    }

    private final function resetOutBuffer()
    {
        $this->_outBuffer = '';
    }

    private final function resetInBuffer()
    {
        $this->_inBuffer = '';
    }

    protected final function addToOutBuffer($data)
    {
        $this->_outBuffer .= $data;
    }

    private final function write()
    {
        $bytes = \socket_write($this->_amiSocket, $this->_outBuffer);
        if ($bytes === false) {
            $errorCode = \socket_last_error();
            $errorMsg = \socket_strerror($errorCode);
            $this->printError(sprintf('Socket writing failed [%s]: %s', $errorCode, $errorMsg));
            $this->reconnect();
        } else {
            $this->resetOutBuffer();
        }
    }

    private final function read()
    {
        $buf = '';
        $bytes = @\socket_recv($this->_amiSocket, $buf, 1000000, MSG_DONTWAIT);//don't send errors to STDOUT
        if ($bytes === false) {
            $errorCode = \socket_last_error();
            $errorMsg = \socket_strerror($errorCode);
            $this->printError(sprintf('Socket reading failed [%s]: %s', $errorCode, $errorMsg));
            $this->reconnect();
        } elseif ($bytes == 0) {
            $this->printError('0 bytes recieved from socket');
            $this->reconnect();
        } else {
            $this->_inBuffer .= $buf;
        }
    }

    private final function parseBuffer()
    {
        while (strpos($this->_inBuffer, "\r\n\r\n")) {
            $this->_messages[] = $this->composeMessageData(substr($this->_inBuffer, 0, strpos($this->_inBuffer, "\r\n\r\n")));
            $this->_inBuffer = substr($this->_inBuffer, strpos($this->_inBuffer, "\r\n\r\n") + 4);
        }
    }

    private final function composeMessageData($messageRaw)
    {
        $messageLines = explode("\r\n", $messageRaw);
        $message = [];
        foreach ($messageLines as $line) {
            if (strpos($line, ': ') !== false) {
                list($key, $value) = explode(': ', $line);
                $key = trim($key);
                $value = trim($value);
                if ($key) {
                    $message[$key] = $value;
                }
            }
        }
        return $message;

    }

    public final function run()
    {
        while ($this->isSessionActive()) {
            $this->fetchOutMessage();
            $this->write();
            $read = array($this->_amiSocket);
            $write = NULL;
            $except = NULL;
            $socketsAvaliable = \socket_select($read, $write, $except, 100);
            if ($socketsAvaliable > 0) {
                $this->read();
                $this->parseBuffer();
                foreach ($this->_messages as $message) {
                    $this->onMessageIn($message);
                }
                $this->_messages = array();
            } elseif ($socketsAvaliable == 0) {
                $this->printMessage('Nothing interesting on socket');
                $this->addToOutBuffer("Action: Ping\n");
                $this->write();
            } elseif ($socketsAvaliable === false) {
                $this->printError(\socket_strerror(socket_last_error()));
            }
        }
    }

    abstract protected function onMessageIn($message);

    abstract protected function fetchOutMessage();
}

?>
