<?php
namespace console\extension;
error_reporting(E_ALL & ~E_NOTICE);
use console\extension\AsteriskAmiTransport;
//use common\models\AveEvent;
//use common\extension\data;

class AsteriskListener extends AsteriskAmiTransport
{

    private function logging($mes)
    {
        date_default_timezone_set('UTC');
        $file = 'AsteriskListener.log';
        $current = date("Y-m-d H:i:s") . '( ' . time() . ' )' . "\n";
        $current .= print_r($mes, true) . "\n";
        $current .= date("Y-m-d H:i:s") . '  ' . "\n\n";
        file_put_contents($file, $current, FILE_APPEND | LOCK_EX);
    }

    static function sendRquest($url, $content = '')
    {
        try {
            $curlHandler = curl_init();
            curl_setopt($curlHandler, CURLOPT_URL, $url);
            curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curlHandler, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curlHandler, CURLOPT_POST, true);
            curl_setopt($curlHandler, CURLOPT_POSTFIELDS, json_encode($content));
            curl_setopt($curlHandler, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curlHandler, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curlHandler, CURLOPT_TIMEOUT, 2);
            curl_setopt($curlHandler, CURLOPT_CONNECTTIMEOUT, 2);
            $responseBody = curl_exec($curlHandler);
            curl_close($curlHandler);
            return $responseBody;
        } catch (Exception $e) {
            echo 'Выброшено исключение #3: ', $e->getMessage(), "\n";
        }
    }

    protected final function onMessageIn($message)
    {
        echo print_r($message);
        if (@$message['Event']=='Newchannel'
           // || @$message['Event']=='NewCallerid'
           // || @$message['Event']=='NewConnectedLine'
            || @$message['Event']=='DialBegin'
           // || @$message['Event']=='DialEnd'
            || @$message['Event']=='BridgeEnter'
            || @$message['Event']=='HangupRequest'
           // || @$message['Event']=='BridgeLeave'
            || @$message['Event']=='Hangup'
            || @$message['Event']=='DeviceStateChange'
            || @$message['Event']=='SoftHangupRequest'
            || @$message['Event']=='VarSet'

        ) {
            self::sendRquest('http://ave.local/api/amievent',$message);

         /*   $client = \Yii::$app->clickhouse;
            $insert = $client->createCommand(null)
                ->insert('ave_event', [
                    'ava_event_id' => data::GUID(),
                    'ava_event_microtime' => \microtime(true),
                    'ava_event_event' => $message['Event'],
                    'ava_event_uniqueid' => $message['Uniqueid'],
                    'ava_event_linkedid' => $message['Linkedid'],
                    'ava_event_text' => json_encode($message),
                    'ava_event_date' => \Yii::$app->pbxDateTimeGTM(),
                ])
                ->execute();
        */
        }
    }

    protected final function fetchOutMessage()
    {

    }


}
?>
